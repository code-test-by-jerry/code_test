# Welcomm to the Jerry's Gitlab


## Who I am?
<center><img src="https://user-images.githubusercontent.com/54479819/64472272-995fc200-d196-11e9-8e2e-39001540da44.jpg" width="400" hight="400"></center>

Hello. This is Jerry.<br>
I am an **_embedded system engineer_** and deal with **_Firmware_**,
**_BSP_** based on Micom or Linux.<br>
I use the **_C language_**, shell script(bash), python and
a little bit C++, ASM.<br>

The one of my hobbies is coding using my laptop.<br>
So, this folder is to arranged the my source codes from 2006 to nowadays.<br>
If you have questions, anyway feedback to me.<br>
Thanks :)

Click my blog	 : **_[Jerry's blog](https://blog.naver.com/snim008)_**<br>
Click my resume	:
**_[LinkedIn](https://www.linkedin.com/in/leejaeseong-19871224)_**


### Education
* **Yonsei University** - MSc 2016 Dept. of Computer Science
* **Dankook University** - BSc 2014 Dept. of Electronic Engineering


### Experiences
* **Intel Corporation** - Application Engineer(Apr.2018~Present)
* **LG Electronics** - Junior Research Engineer(Feb.2016~Apr.2018)
* **Mobile and Embedded System Research Group** - MSc student(Dec.2013~Dec.2015)
* **MAZE refer to a research of micro robot** - BSc student(Mar.2006~Feb.2014)
* **Cuberevo-digital co., ltd** - Software Engineer (Feb.2009~Dec.2011)
