#include <stdio.h>

static inline int _get_binary(int input, char bin[])
{
    int len = 0;

    while (1)
    {
        if (input <= 1)
            break;

        bin[len++] = input % 2;
        input >>= 1;
    }
    bin[len++] = input;

    return len;
}

static inline int _get_zero_count(const char bin[], int len)
{
    int i;
    for (i = 0; i < len; i++)
    {
        if (bin[i] == 1)
            break;
    }

    int max = 0, count = 0;
    for (; i < len; i++)
    {
        if (bin[i] == 0)
            count++;
        else
        {
            if (max < count)
                max = count;
            count = 0;
        }
    }

    return max;
}

int solution(int N)
{
    if (N < 5)
        return 0;

    char binary[32] = {
        0,
    };
    int len = _get_binary(N, binary);

    return _get_zero_count(binary, len);
}

int main(void)
{
    printf("%d\n", solution(1041));
    printf("%d\n", solution(15));
    printf("%d\n", solution(32));
    printf("%d\n", solution(529));
    printf("%d\n", solution(20));
    printf("%d\n", solution(2147483647));

    printf("%d\n", solution(2));
    printf("%d\n", solution(1));
    printf("%d\n", solution(3));

    return 0;
}
