#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static int _rotate_array(int arr[], int len, int loop)
{
    if (len == loop || len <= 1)
        return 0;

    if (len < loop)
        loop %= len;

    int *temp = calloc(1, sizeof(int) * len);
    if (temp == NULL)
    {
        printf("[ERR] Failed to memory allocate.\n");
        exit(-1);
    }

    memcpy(&temp[0], &arr[len - loop], sizeof(int) * (size_t)loop);
    memcpy(&temp[loop], &arr[0], sizeof(int) * (size_t)(len - loop));
    memcpy(&arr[0], &temp[0], sizeof(int) * (size_t)len);

    free(temp);

    return 0;
}

struct Results solution(int A[], int N, int K)
{
    _rotate_array(A, N, K);

    struct Results result = {.A = A, .N = N};
    return result;
}
