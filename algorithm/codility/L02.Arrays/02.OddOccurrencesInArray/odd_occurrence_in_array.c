#include <stdio.h>
#include <stdlib.h>

static int _compare_func(const void *first, const void *second)
{
    int first_num = *(int *)first;
    int second_num = *(int *)second;

    if (first_num > second_num)
        return 1;
    else if (first_num < second_num)
        return -1;
    else
        return 0;
}

int solution(int A[], int N)
{
    if (N == 1)
        return A[0];

    qsort(A, N, sizeof(int), _compare_func);

    int cnt = 0;
    int compare = 0;
    for (int i = 0; i < N; i++)
    {
        if (compare != A[i])
        {
            if (cnt & 0x1)
                return compare;

            compare = A[i];
            cnt = 0;
        }
        cnt++;
    }

    return A[N - 1];
}
