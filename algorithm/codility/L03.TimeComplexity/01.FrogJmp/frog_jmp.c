#include <stdio.h>

int solution(int X, int Y, int D)
{
    int final = Y - X;
    int count = (final / D);
    count += (final % D ? 1 : 0);

    return count;
}

int main(void)
{
    printf("%d\n", solution(10, 85, 30));

    return 0;
}
