#include <stdio.h>

int solution(int A[], int N)
{
    if (N == 0)
        return 1;

    unsigned long long num = (unsigned long long)N;
    unsigned long long sum = ((num + 1) * (num + 2)) >> 1;

    for (int i = 0; i < N; i++)
        sum -= (unsigned long long)(A[i]);

    return (int)sum;
}

int main(void)
{
    int A[] = {2, 3, 1, 5};

    printf("%d\n", solution(A, 4));

    return 0;
}
