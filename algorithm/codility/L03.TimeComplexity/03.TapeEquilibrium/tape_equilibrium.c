#include <stdio.h>
#include <stdlib.h>

int solution(int A[], int N)
{
    int i;
    int all_sum = 0;
    for (i = 0; i < N; i++)
        all_sum += A[i];

    int min = 100000001;
    int part_sum = 0;
    int part_result, abs_result;
    for (i = 0; i < N - 1; i++)
    {
        part_sum += A[i];
        part_result = all_sum - part_sum;

        abs_result = abs(part_sum - part_result);
        if (min > abs_result)
            min = abs_result;
    }

    return min;
}

int main(void)
{
    int A[] = {3, 1, 2, 4, 3};

    printf("%d\n", solution(A, 5));

    return 0;
}
