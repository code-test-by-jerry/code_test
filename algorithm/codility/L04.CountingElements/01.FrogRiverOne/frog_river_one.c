#include <stdio.h>
#include <stdlib.h>

int solution(int X, int A[], int N)
{
    if (N == 1)
    {
        return (A[0] == X) ? 0 : -1;
    }

    int *cnt_arr = calloc(1, sizeof(int) * N);
    if (cnt_arr == NULL)
    {
        printf("[ERR] Failed to allocate the memory.\n");
        exit(-1);
    }

    int cnt = 0;
    for (int i = 0; i < N; i++)
    {
        if (cnt_arr[A[i]] == 0)
        {
            cnt_arr[A[i]] = 1;
            cnt++;
        }

        if (cnt == X)
            return i;
    }

    free(cnt_arr);
    return -1;
}

#define GET_ARRARY_LEN(arr) (sizeof(arr) / sizeof(arr[0]))

int main(void)
{
    int A[] = {1, 3, 1, 4, 2, 3, 5, 4};

    printf("%d\n", solution(5, A, GET_ARRARY_LEN(A)));

    return 0;
}
