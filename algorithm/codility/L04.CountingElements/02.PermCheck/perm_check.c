#include <stdio.h>
#include <stdlib.h>

int solution(int A[], int N)
{
    if (N == 1)
        return (A[0] == 1) ? 1 : 0;

    int *arr_cnt = calloc(1, sizeof(int) * N);
    if (arr_cnt == NULL)
    {
        printf("[ERR] Failed to allocate memory.\n");
        exit(-1);
    }

    int idx;
    int cnt = 0;
    for (int i = 0; i < N; i++)
    {
        idx = A[i] - 1;
        if (idx >= N)
            continue;

        if (arr_cnt[idx] == 0)
        {
            arr_cnt[idx] = 1;
            cnt++;
        }
    }

    free(arr_cnt);
    return (cnt == N) ? 1 : 0;
}

int main(void)
{
    int A[] = {4, 1, 3, 2};
    int B[] = {4, 1, 3};

    printf("%d\n", solution(A, 4));
    printf("%d\n", solution(B, 3));

    return 0;
}
