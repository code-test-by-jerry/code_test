#include <algorithm>
#include <iostream>
#include <vector>

std::vector<int> solution(int N, std::vector<int> &A)
{
    std::vector<int> return_vector(N);

    int idx;
    int max = 0;
    int loop = A.size();

    int last_max = 0;
    for (int i = 0; i < loop; i++)
    {
        if (A[i] > N)
        {
            last_max = max;
        }
        else
        {
            idx = A[i] - 1;
            if (return_vector[idx] < last_max)
                return_vector[idx] = last_max + 1;
            else
                return_vector[idx]++;

            if (max < return_vector[idx])
                max = return_vector[idx];
        }
    }

    for (int i = 0; i < N; i++)
    {
        if (return_vector[i] < last_max)
            return_vector[i] = last_max;
    }

    return return_vector;
}

int main(void)
{
    std::vector<int> A = {3, 4, 4, 6, 1, 4, 4};
    std::vector<int> ret = solution(5, A);

    for (auto it = ret.begin(); it < ret.end(); it++)
        std::cout << *it;
    std::cout << std::endl;

    return 0;
}
