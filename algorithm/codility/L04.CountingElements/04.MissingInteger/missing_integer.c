#include <stdio.h>
#include <stdlib.h>

int solution(int A[], int N)
{
    if (N == 1)
        return (A[0] == 1) ? 2 : 1;

    int arr_cnt[1000001] = {
        0,
    };

    int i;
    int max = 0;
    for (i = 0; i < N; i++)
    {
        if (A[i] < 0)
            continue;

        if (arr_cnt[A[i]] == 0)
        {
            arr_cnt[A[i]] = 1;
            if (max < A[i])
                max = A[i];
        }
    }

    if (max == 0)
        return 1;

    for (i = 1; i <= max; i++)
    {
        if (arr_cnt[i] == 0)
            return i;
    }

    return i;
}

#define GET_ARR_LEN(arr) (sizeof(arr) / sizeof(arr[0]))

int main(void)
{
    int test1[] = {1, 3, 6, 4, 1, 2};
    int test2[] = {1, 2, 3};
    int test3[] = {-1, -3};

    printf("%d\n", solution(test1, GET_ARR_LEN(test1)));
    printf("%d\n", solution(test2, GET_ARR_LEN(test2)));
    printf("%d\n", solution(test3, GET_ARR_LEN(test3)));

    return 0;
}
