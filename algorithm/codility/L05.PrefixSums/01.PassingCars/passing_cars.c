#include <stdio.h>

int solution(int A[], int N)
{
    if (N == 1)
        return 0;

    int i;
    for (i = 0; i < N; i++)
    {
        if (A[i] == 0)
            break;
    }

    if (i == N)
        return 0;

    int ratio = 0;
    int sum = 0;
    for (; i < N; i++)
    {
        if (A[i] == 0)
            ratio++;
        else
            sum += ratio;

        if (sum > 1000000000)
            return -1;
    }

    return sum;
}

#define GET_ARR_LEN(arr) (sizeof(arr) / sizeof(arr[0]))

int main(void)
{
    int test1[] = {0, 1, 0, 1, 1};

    printf("%d\n", solution(test1, GET_ARR_LEN(test1)));

    return 0;
}
