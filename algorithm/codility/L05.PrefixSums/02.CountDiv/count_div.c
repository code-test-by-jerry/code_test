#include <stdio.h>

static inline int _check_mod_count(int numerator, int denominator)
{
    return (numerator / denominator) + 1;
}

int solution(int A, int B, int K)
{
    int count = _check_mod_count(B, K);
    return (A == 0) ? count : count - _check_mod_count(A - 1, K);
}

int main(void)
{
    printf("%d\n", solution(6, 11, 2));
    return 0;
}
