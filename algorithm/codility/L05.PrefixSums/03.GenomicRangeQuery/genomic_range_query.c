#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Results
{
    int *A;
    int M; // Length of the array
};

struct Results solution(char *S, int P[], int Q[], int M)
{
    int *ret = malloc(sizeof(int) * M);
    struct Results result = {ret, M};

    int char_to_int[128] = {
        0,
    };
    char_to_int['A'] = 1;
    char_to_int['C'] = 2;
    char_to_int['G'] = 3;
    char_to_int['T'] = 4;

    int i;
    int len = strlen(S);
    int save[100001][5] = {
        {
            0,
        },
    };

    save[1][char_to_int[(int)S[0]]]++;
    for (i = 2; i < len + 1; i++)
    {
        save[i][1] = save[i - 1][1];
        save[i][2] = save[i - 1][2];
        save[i][3] = save[i - 1][3];
        save[i][4] = save[i - 1][4];
        save[i][char_to_int[(int)S[i - 1]]]++;
    }

    for (i = 0; i < M; i++)
    {
        for (int j = 1; j < 5; j++)
        {
            if (save[P[i]][j] != save[Q[i] + 1][j])
            {
                result.A[i] = j;
                break;
            }
        }
    }

    result.M = M;
    return result;
}

int main(void)
{
    int M = 3;
    int P[] = {2, 5, 0};
    int Q[] = {4, 5, 6};
    char *S = "CAGCCTA";

    struct Results ret = solution(S, P, Q, M);

    for (int i = 0; i < ret.M; i++)
        printf("%d ", ret.A[i]);
    printf("\n");

    free(ret.A);

    return 0;
}
