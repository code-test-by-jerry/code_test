#include <stdio.h>

int solution(int A[], int N)
{
    int idx = 0;
    double min = 20001.0;
    double avg = (A[0] + A[1]) / (double)2.0;
    if (min > avg)
        min = avg;

    for (int i = 2; i < N; i++)
    {
        avg = (A[i] + A[i - 1] + A[i - 2]) / (double)3.0;
        if (min > avg)
        {
            min = avg;
            idx = i - 2;
        }

        avg = (A[i] + A[i - 1]) / (double)2.0;
        if (min > avg)
        {
            min = avg;
            idx = i - 1;
        }
    }

    return idx;
}

#define GET_ARR_LEN(arr) (sizeof(arr) / sizeof(arr[0]))

int main(void)
{
    int test1[] = {5, 6, 3, 4, 9};
    printf("%d\n", solution(test1, GET_ARR_LEN(test1)));

    int test2[] = {4, 2, 2, 5, 1, 5, 8};
    printf("%d\n", solution(test2, GET_ARR_LEN(test2)));

    int test3[] = {10000, -10000};
    printf("%d\n", solution(test3, GET_ARR_LEN(test3)));

    return 0;
}
