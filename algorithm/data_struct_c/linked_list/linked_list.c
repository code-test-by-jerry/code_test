#include "./linked_list.h"

struct linked_list
{
    int data;
    struct linked_list *ptr;
};

list_t *create_list(void)
{
    list_t *new = malloc(sizeof(list_t));
    FORMULA_GUARD(new == NULL, NULL, ERR_MEMORY_SHORTAGE);

    printf("[new] %p\n", new);

    new->data = 0;
    new->ptr = NULL;

    return new;
}

int destroy_list(list_t *del)
{
    FORMULA_GUARD(del == NULL, -EPERM, ERR_INVALID_PTR);

    printf("[del] %p\n", del);
    PTR_FREE(del);

    return 0;
}

int set_data(list_t *list, int set_data)
{
    FORMULA_GUARD(list == NULL, -EPERM, ERR_INVALID_PTR);
    list->data = set_data;

    return 0;
}

int set_ptr(list_t *list, list_t *set_ptr)
{
    FORMULA_GUARD(list == NULL, -EPERM, ERR_INVALID_PTR);
    list->ptr = set_ptr;

    return 0;
}

int get_data(const list_t *list)
{
    FORMULA_GUARD(list == NULL, -EPERM, ERR_INVALID_PTR);
    return list->data;
}

list_t *get_ptr(const list_t *list)
{
    FORMULA_GUARD(list == NULL, NULL, ERR_INVALID_PTR);
    return list->ptr;
}

int delete_all_list(list_t *head)
{
    FORMULA_GUARD(head == NULL, -EPERM, ERR_INVALID_PTR);

    list_t *del;
    while (head)
    {
        del = head;
        head = head->ptr;
        destroy_list(del);
    }

    return 0;
}

int print_all_list(const list_t *head)
{
    FORMULA_GUARD(head == NULL, -EPERM, ERR_INVALID_PTR);

    for (int i = 0; head; head = head->ptr, i++)
        printf("[%3d] %d\n", i, head->data);

    return 0;
}

int main(void)
{
    list_t *head = NULL;
    list_t *cur = NULL;

    for (int i = 0; i < 10; i++)
    {
        cur = head;
        while (cur && cur->ptr)
            cur = cur->ptr;

        list_t *new = create_list();
        set_data(new, i);
        if (head == NULL)
            head = new;
        else
            set_ptr(cur, new);
    }

    print_all_list(head);
    delete_all_list(head);

    return 0;
}
