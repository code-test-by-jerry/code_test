/* vim: set tabstop=4:softtabstop=4:shiftwidth=4:ffs=unix */
#ifndef LINKED_LIST_H
#define LINKED_LIST_H

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ERR_INVALID_PTR "Invalid Pointer."
#define ERR_INVALID_RET "Invalild return value."
#define ERR_MEMORY_SHORTAGE "Failed to allocate the memory."

#ifdef __GNUC__
#define unlikely(x) __builtin_expect(!!(x), 0)
#else
#define unlikely(x) (x)
#endif

#define ERR_MSG(fmt, ...)                                                      \
    do                                                                         \
    {                                                                          \
        fprintf(stderr, "[err] " fmt " / func : %s, line : %d\n",              \
                ##__VA_ARGS__, __func__, __LINE__);                            \
    } while (0)

#define FORMULA_GUARD(formula, ret, msg, ...)                                  \
    do                                                                         \
    {                                                                          \
        if (unlikely(formula))                                                 \
        {                                                                      \
            if (strcmp(msg, ""))                                               \
                ERR_MSG(msg, ##__VA_ARGS__);                                   \
            return ret;                                                        \
        }                                                                      \
    } while (0)

#define PTR_FREE(ptr)                                                          \
    do                                                                         \
    {                                                                          \
        if (ptr)                                                               \
        {                                                                      \
            free(ptr);                                                         \
            ptr = NULL;                                                        \
        }                                                                      \
    } while (0)

typedef struct linked_list list_t;

/* init & exit */
list_t *create_list(void);
int destroy_list(list_t *del);

/* setter */
int set_data(list_t *list, int set_data);
int set_ptr(list_t *list, list_t *set_ptr);

/* getter */
int get_data(const list_t *list);
list_t *get_ptr(const list_t *list);

/* utils */
int delete_all_list(list_t *head);
int print_all_list(const list_t *head);

#endif
