#!/bin/bash

DEPEND="linked_list"

cd "../${DEPEND}" || exit

make clean
make

if [ ! -f "${DEPEND}.so" ]
then
	echo "Failed to create the ${DEPEND}.so!"
	exit
fi

cd - || exit
make clean
make

echo "Done!"
