#include <linked_list.h>

typedef struct queue
{
    list_t *front;
    list_t *rear;
} que_t;

static que_t *_init_queue(void)
{
    que_t *new = calloc(1, sizeof(que_t));
    FORMULA_GUARD(new == NULL, NULL, ERR_MEMORY_SHORTAGE);

    return new;
}

static int _exit_queue(que_t *del)
{
    FORMULA_GUARD(del == NULL, -EPERM, ERR_INVALID_PTR);
    PTR_FREE(del);

    return 0;
}

static int _enqueue(que_t *queue, int data)
{
    FORMULA_GUARD(queue == NULL, -EPERM, ERR_INVALID_PTR);

    list_t *new = create_list();
    FORMULA_GUARD(new == NULL, -EPERM, ERR_MEMORY_SHORTAGE);

    set_data(new, data);
    set_ptr(new, NULL);

    if (queue->front == NULL)
        queue->front = new;
    else
        set_ptr(queue->rear, new);

    queue->rear = new;

    return 0;
}

static int _dequeue(que_t *queue)
{
    FORMULA_GUARD(queue == NULL, -EPERM, ERR_INVALID_PTR);

    list_t *del = queue->front;
    FORMULA_GUARD(del == NULL, -EPERM, "Empty status in the queue!");

    queue->front = get_ptr(queue->front);
    printf("[Dequeue] %d\n", get_data(del));

    int ret = destroy_list(del);
    FORMULA_GUARD(ret < 0, -EPERM, "");

    return 0;
}

int main(void)
{
    que_t *que = _init_queue();

    int ret;
    for (int i = 0; i < 10; i++)
    {
        ret = _enqueue(que, i + 1);
        FORMULA_GUARD(ret < 0, -EPERM, "");
    }

    while ((ret = _dequeue(que)) >= 0)
        ;

    _exit_queue(que);

    return 0;
}
