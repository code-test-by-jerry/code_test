#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#include <linked_list.h>

typedef struct stack
{
    list_t *top;
} stack_t;

static stack_t *_init_stack(void)
{
    stack_t *new = calloc(1, sizeof(stack_t));
    FORMULA_GUARD(new == NULL, NULL, ERR_MEMORY_SHORTAGE);

    return new;
}

static int _clear_stack(stack_t *sp)
{
    FORMULA_GUARD(sp == NULL, -EPERM, ERR_INVALID_PTR);
    PTR_FREE(sp);

    return 0;
}

static int _push(stack_t *sp, int data)
{
    FORMULA_GUARD(sp == NULL, -EPERM, ERR_INVALID_PTR);

    list_t *new = create_list();
    FORMULA_GUARD(new == NULL, -EPERM, "");

    set_data(new, data);

    if (sp->top)
        set_ptr(new, sp->top);

    sp->top = new;

    return 0;
}

static int _pop(stack_t *sp)
{
    FORMULA_GUARD(sp == NULL, -EPERM, ERR_INVALID_PTR);

    list_t *del = sp->top;
    FORMULA_GUARD(del == NULL, -EPERM, "Empty status in the stack!");

    sp->top = get_ptr(del);
    printf("[Pop] %d\n", get_data(del));

    int ret = destroy_list(del);
    FORMULA_GUARD(ret < 0, -EPERM, "");

    return 0;
}

int main(void)
{
    stack_t *sp = _init_stack();
    FORMULA_GUARD(sp == NULL, -EPERM, "");

    int ret;
    for (int i = 0; i < 10; i++)
    {
        ret = _push(sp, i + 1);
        FORMULA_GUARD(ret < 0, -EPERM, "");
    }
    printf("\n");

    while ((ret = _pop(sp)) >= 0)
        ;
    _clear_stack(sp);

    return 0;
}
