#include <stdio.h>

static int func(void)
{
    printf("Hello World!!\n");
    return 0;
}

int main(void)
{
    printf("Hello World!\n");

    func();

    return 0;
}
