#include <stdio.h>

int main(void)
{
    int num = 3;
    int *pnum = NULL;
    printf("%p %p\n", &num, pnum);

    pnum = num;
    printf("%p %p\n", &num, pnum);

    pnum = 1234;
    printf("%p %p\n", &num, pnum);

    pnum = &num;
    printf("%p %p\n", &num, pnum);

    *pnum = 5;
    printf("%p %p %d %d\n", &num, pnum, num, *pnum);

    double double_value = 12.4;
    pnum = &double_value;
    printf("%p %p %.2lf %.2lf\n", &double_value, pnum, double_value, *pnum);

    printf("%.2lf\n", *(double *)pnum);
    printf("%p\n", &pnum);

    return 0;
}
