#include <stdio.h>

//#define CALL_BY_REF

#ifdef CALL_BY_REF
void func(int *num1, int *num2)
{
    int temp = *num1;
    *num1 = *num2;
    *num2 = temp;

    printf("[%s] %p %p\n", __func__, num1, num2);
    printf("[%s] %d %d\n", __func__, *num1, *num2);
}
#else
void func(int num1, int num2)
{
    int temp = num1;
    num1 = num2;
    num2 = temp;

    printf("[%s] %p %p\n", __func__, &num1, &num2);
    printf("[%s] %d %d\n", __func__, num1, num2);
}
#endif

int main(void)
{
    int num1 = 3;
    int num2 = 6;

    printf("[%s] %p %p\n", __func__, &num1, &num2);
    printf("[%s] %d %d\n", __func__, num1, num2);

#ifdef CALL_BY_REF
    func(&num1, &num2);
#else
    func(num1, num2);
#endif

    printf("[%s] %d %d\n", __func__, num1, num2);

    return 0;
}
