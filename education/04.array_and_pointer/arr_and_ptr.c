#include <stdio.h>

int main(void)
{
    int arr1[4];

    printf("%p %p\n", arr1, &arr1[0]);
    printf("%lu %lu\n", sizeof(arr1), sizeof(&arr1[0]));

    printf("%lu %lu %lu\n", sizeof(int), sizeof(int *), sizeof(long));
    printf("%p %p %p\n", arr1 + 1, &arr1[0] + 1, &arr1[1]);

    arr1[1] = 4;

    printf("%d %d %d\n", *(arr1 + 1), *(&arr1[0] + 1), arr1[1]);

    int *ptr = arr1;
    printf("%p %d %d\n", ptr + 1, *(ptr + 1), ptr[1]);

    ///////////////////////////////////////////////////////////////////////////

    int cnt = 1;
    int arr2[2][3];

    printf("\n@@@@\n");
    for (int i = 0; i < 2; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            arr2[i][j] = cnt++;
            printf("[%d][%d] %p %d\t", i, j, &arr2[i][j], arr2[i][j]);
        }
        printf("\n");
    }
    printf("\n");

    printf("%p %d\n", &arr2[1][2], arr2[1][2]);
    printf("%p %p %p\n", arr2, arr2 + 1, arr2 + 2);
    printf("%p %p %p\n", *arr2, *arr2 + 1, *arr2 + 2);

    printf("%lu %lu %lu\n", sizeof(arr2), sizeof(*arr2), sizeof(**arr2));

    printf("%d %d %d\n", arr2[1][2], *(arr2[1] + 2), *(*(arr2 + 1) + 2));

    ///////////////////////////////////////////////////////////////////////////

    int *ptr_arr[3];
    int(*arr_ptr)[3];

    printf("\n%lu %lu\n\n", sizeof(ptr_arr), sizeof(arr_ptr));

    return 0;
}
