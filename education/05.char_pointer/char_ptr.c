#include <stdio.h>

int main(void)
{
    char str1[] = "Hello World";
    //    char str2[] = "Hello World";
    char *str2 = "Hello World";

    printf("%s %s\n", str1, str2);

    str1[6] = 'w';
    str2[6] = 'w';

    printf("%s %s\n", str1, str2);

    return 0;
}
