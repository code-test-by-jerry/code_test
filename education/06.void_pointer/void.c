#include <stdio.h>

int func(void *data)
{
    printf("%p %d %x\n", data, *(int *)data, *(int *)data);
    return 0;
}

int main(void)
{
    char num1;
    int num2;
    float num3;
    double num4;

    num1 = 3;

    printf("%p %d\n", &num1, num1);

    func(&num1);

    return 0;
}
