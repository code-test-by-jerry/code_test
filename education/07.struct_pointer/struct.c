#include <stdio.h>

struct test
{
    int arr[100];
};

struct struct_size
{
    int num1;
    float num2;
    double num3;
};

union union_size {
    int num1;
    float num2;
    double num3;
};

int func_1(struct test test_1, struct test *test_2)
{
    printf("%lu %lu\n", sizeof(test_1), sizeof(test_2));

    return 0;
}

int main(void)
{
    printf(" %lu %lu\n", sizeof(int), sizeof(struct test));

    struct test test;
    //    func_1(test, &test);

#if 0
    printf("%lu %lu %lu\n", sizeof(int), sizeof(float), sizeof(double));
    printf("%lu %lu\n", sizeof(struct struct_size), sizeof(union union_size));
#endif

    return 0;
}
