#include <stdio.h>

int func_1(int num1, float num2)
{
    printf("[%s] %d %.2f\n", __func__, num1, num2);
    return 0;
}

static int func_2(int num1, float num2)
{
    printf("[%s] %d %.2f\n", __func__, num1, num2);
    return 0;
}

int main(void)
{
    int num1 = 1;
    float num2 = 2.4;

    func_1(num1, num2);
    func_2(num1, num2);

    printf("%p %p\n", func_1, func_2);

#if 0
    int (*pfunc1)(int, float) = func_1;
    int (*pfunc2)(int, float) = func_2;
    printf("%p %p\n", pfunc1, pfunc2);

//    pfunc1(num1, num2);
//    pfunc2(num1, num2);
#endif

    return 0;
}
