#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    //    int arr[10];

#if 0
	// int arr[?];

    int num;
    printf("input num: ");
    scanf("%d", &num);

    int arr[num];
#endif

#if 1
    int *arr;
    printf("%p\n", arr);

    arr = malloc(sizeof(int) * 10);
    printf("%p\n", arr);
#endif

#if 0
    for (int i = 0; i < 10; i++)
    {
        arr[i] = i;
        printf("%d\t", arr[i]);
    }
    printf("\n");
#endif

#if 0
    free(arr);
    printf("%p\n", arr);
#endif

#if 0
    if (arr)
    {
        free(arr);
        arr = NULL;
    }
#endif

    return 0;
}
