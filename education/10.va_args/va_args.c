#include <stdarg.h>
#include <stdio.h>

int func(int num, ...)
{
    int ret;
    va_list va;

    va_start(va, num);

    for (int i = 0; i < num; i++)
    {
        ret = va_arg(va, int);
        printf("%d\t", ret);
    }
    printf("\n");

    va_end(va);

    return 0;
}

int main(void)
{
    func(2, 13, 29);
    func(10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

    func(2, 5, 6, 7);
    func(4, 1, 2);

    return 0;
}
