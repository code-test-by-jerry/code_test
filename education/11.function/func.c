#include <stdio.h>

//#define DEFAULT
//#define ARGS

int func(int x) { return (3 * x) + 5; }

#ifdef DEFAULT
int main(void)
{
    int num;

    printf("Input the Number: ");
    scanf("%d", &num);

    printf("result: %d\n", func(num));
    return 0;
}
#elif ARGS
int main(int argc, char *argv[])
{
    int num;

    printf("Input the Number: ");
    scanf("%d", &num);

    printf("result: %d\n", func(num));
    return 0;
}
#else
int main(int argc, char *argv[])
{
    for (int i = 0; i < argc; i++)
        printf("%d %s\n", i, argv[i]);

    return 0;
}
#endif
