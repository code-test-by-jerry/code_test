#include <errno.h>
#include <stdio.h>

static inline char _compute_grade(int score)
{
    if (score < 0 || score > 100)
    {
        printf("Please enter only 0 to 100 numbers.\n");
        return -EPERM;
    }

    const char grade_table[] = "FDCBAA";
    int index = (score / 10) - 5;
    if (index < 0)
        index = 0;

    return grade_table[index];
}

int main(void)
{
    int score;
    printf("Inpute the Score: ");
    scanf("%d", &score);

    char ret = _compute_grade(score);
    if (ret != -EPERM)
        printf("Grade: %c\n", ret);

    return 0;
}
