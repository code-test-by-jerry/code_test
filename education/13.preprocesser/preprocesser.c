#include <stdio.h>

#include "preprocesser.h"

#if 1
#define MAX_BUF 128
#else
enum
{
    MAX_BUF = 128
};
#endif

struct data
{
    int num1;
    double num2;
};

int func(int num)
{
    printf("%d\n", num);
    return 0;
}

int main(void)
{
    printf("Hello World!\n");

    func(3);
    return 0;
}
