#include <stdio.h>

int main(void)
{
    int num1 = 0;
    int num2 = 4;
    int num3 = 5;

    num1 = num2 + num3;
    printf("%d\n", num1);

    num1 = num2 - num3;
    printf("%d\n", num1);

    num1 = num2 * num3;
    printf("%d\n", num1);

    num1 = num2 / num3;
    printf("%d\n", num1);

    return 0;
}
