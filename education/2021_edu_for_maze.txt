0. 메모리에 대한 이해
 - 물리 메모리(register, cache, RAM, ROM)와 가상 메모리(stack, heap, data, code(text), bss)
 - 지역변수(파라미터), 전역변수, 함수, static 변수와 함수에서의 메모리 구조, const를 붙였을 때의 메모리 위치

1. 포인터 기초
 - 포인터의 정의, 선언, 주소와 값의 차이 (메모리 관점) &, *의 역할
 - NULL 이란

2. 파라미터 포인터
 - swap(3, 4) 문제
 - call by value, call by reference의 의미

3. 배열과 포인터
 - 1차원 배열과 포인터의 상관 관계, 메모리 관점에서 그림
 - []과 *(주소)의 관계
 - 2차원 배열 [][] 은 어떻게 표시할 수 있는가?
 - 2차원 포인터와 2차원 배열의 관계는?
 - 배열 포인터와 포인터 배열의 차이점

4. 문자열과 포인터
 - char *선언 방식, %s와 %c에 대한 고찰
 - *str과 str[]의 차이점 -> str[1] = 'a'; 일 때 상황 예측, 메모리 관점

5. void 포인터 및 캐스팅
 - 자료형의 정의 (int, char, float, double, void)
 - WORD란 무엇인가? 포인터는 어떻게 정의 되는가,,
 - 포인터의 캐스팅, void 포인터를 본다면 어떻게 판단하면 되는가?

6. 구조체와 포인터
 - 구조체란 무엇인가,, (무한대의 배열, 새로운 자료형에 대한 정의, typedef)
 - 구조체의 주소와 구조체 첫 요소의 주소, 값은 같지만 size가 다른 이유, word arrange
 - 구조체를 파라미터로 넘길 때 포인터의 중요성
 - union과 struct 차이점,,

7. 함수 포인터
 - 함수의 이름의 역할, symbol이란, 컴파일러가 함수를 추적하는 방법(메모리 관점)
 - 함수 포인터는 어떻게 쓰는가?
 - 함수 포인터 배열
 - *(volatile unsigned long *)0x1234의 의미 -> GPIO는 어떻게 컨트롤 되는가,, 메모리 관점
 - user program jump

8. 메모리 동적 할당
 - 변수를 선언한다는 개념은 정적 할당
 - 필요로 할 때마다 메모리를 할당 받고 해제 한다 -> heap의 역할
 - malloc(calloc, realloc), free
 - Linked List, Stack, Queue에 대한 구현

9. 가변 인자
 - va_변수란? ##VA_ARGS, ...의 역할
 - 가변 인자를 이용한 코드를 만들어 보자!
 - vfdprintf, sci_printf에 대한 분석

-----------------------------------------------------------------------------------

10. HW 관점에서의 함수
 - y = ax + b 에 대한 f(x) 구성은 어떻게 코드로 표현 되는가
 - 함수가 호출 될 때 stack에서의 움직임
 - 왜 interrupt에서 함수 호출을 줄여야 되는가
 - FILE 포인터 사용법 (fopen, fclose, fgets, fscanf, fprintf, ...)
 - int main(int args, char *argv[])
 - 모듈화의 중요성, 함수의 가독성, 왜 펌쟁이들은 함수를 못짜는가

11. HW관점에서의 조건문, 반복문
 - while과 for문은 어떻게 파이프라인을 망가지게 하는가..
    - fetch - decode - read - excute
    - 하버드 - 폰노이만 , risc - cisc
    - for로 초기화 보단 memset
 - if문과 switch문은 전체 코드의 70%이상을 차지, 이는 코드 속도에 어떤 영향을 미치는가..
 - 반복문 코드의 최적화 -> eeprom 저장 코드를 어떻게 코딩하는가
 - if문은 어떻게 최적화 해야 하는가
    - 규칙을 찾아내서 배열 인덱스로 풀어내는법(점수에 따른 학점 문제)
	- 삼항연산자 -> 포인터 활용법
    - 중첩 if문을 어떻게 풀어야 하는가 (항상 동일한 시간으로 동작되는 코드)
 - goto는 왜 쓰지 말라고 하는가?

12. #define과 enum
 - 각각의 역할, 전처리기와 컴파일러
 - #ifndef ~ #endif의 역할, #undef 란?
 - #progma 역할
 - variables.h 분석
 - 메크로와 inline 함수

13. 연산
 - 사칙연산에 대한 ALU의 동작 환경, 클럭, / 보단 * 보단 + 등등
 - 논리연산과 비트연산은 한 클럭에 done
 - IQmath 라이브러리의 문제점 -> 비트연산으로 해결,,

14. 변수 선언
 - 물리 메모리(register, cache, RAM, ROM)와 가상 메모리(stack, heap, data, code(text), bss)
 - 지역변수(파라미터), 전역변수, 함수, static 변수와 함수에서의 메모리 구조, const를 붙였을 때의 메모리 위치

15. printf와 scanf
 - int printf(const void *fmt, ...); 의 원형의 의미
 - scanf의 위험성, 메모리 버퍼 오버플로우 공격

16. 그 다음은?
 - Makefile 작성법
 - 모니터 프로그램 분석, 어셈블러를 알아야 하는 이유
 - 언매니지드 언어(c/c++) vs 매니지드 언어(JAVA/python/그 외...)
 - 회로, 제어, 통신, 신호 및 시스템, 반도체, 펌웨어
 - 자료구조, 알고리즘, 시스템 프로그래밍, 운영체제, 리눅스
 - 웹(frontend(JS, HTML, CSS) - Angular, React, Vue / backend(PHP, C#, JS, Python, SQL) - Django, Flask, Spring, DB, Apache, Ngnix, Docker, Postman)
   앱(안드로이드 - JAVA, Cotlin / ios - C++, Swift), 게임(수학, C++), 딥러닝(python, 데이터 사이언티스트)

------------------






 - 회로, 제어, 통신, 신호 및 시스템, 반도체, 펌웨어
 - 자료구조, 알고리즘, 시스템 프로그래밍, 운영체제, 리눅스
 - 웹(frontend(JS, HTML, CSS) - Angular, React, Vue / backend(PHP, C#, JS, Python, SQL) - Django, Flask, Spring, DB, Apache, Ngnix, Docker, Postman)
   앱(안드로이드 - JAVA, Cotlin / ios - C++, Swift), 게임(수학, C++), 딥러닝(python, 데이터 사이언티스트)

1. 어셈블, 컴파일, 링킹
2. 시스템에 따라서 다름
3. buffer overflow 문제
4. 전처리기와 컴파일러의 처리 시점
5. *(volatile unsinged long *)0xabcd |= (0x1 << 3)
6. fetch - decode - execute - save를 예측 하지만 이를 반족하지 않을 시 파이프 깨짐
7. 레지스터, 캐시는 내부 / 램, 롬은 외부에 위치, 지역변수, 함수 -> stack, 전역변수 -> data, 동적할당 -> heap, 컴파일된 코드 -> text
8. 3, 5 / 3, 5
9. stack에 저장 or data에 저장 / 함수는 파일 내에서만 사용한다 -> 컴파일 시 심볼 관련,,
10. 3, 5
11. int func(int x) { return 3 * x + 5; }
12. 12, 16
13. 논 캐시, 메모리에 데이터를 직접 RW 한다
14. arr[2]에 3, 컴파일 에러
15. Hello, hello
