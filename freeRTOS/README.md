# How to setup the environment of the FreeRTOS


## Install the prerequisite

  ```bash
  $ sudo apt update
  $ sudo apt install libusb-1.0.0-dev
  ```


## Install the USB driver for the nucleo-64 board

  * refer to the link for stsw-link007
  * [Click the link for the stsw-link007](https://www.st.com/en/development-tools/stsw-link007.html)

  ```bash
  $ unzip en.stsw-link007-v3-9-3_v3.9.9.zip
  $ cd stsw-link007/AllPlatforms/StlinkRulesFilesForLinux
  $ sudo dpkg -i st-stlink-udev-rules-1.0.3-2-linux-all.deb
  $ sudo udevadm control --reload-rules
  ```


## Install the ***stlink***

  * download the source from the Github and install on the localhost

  ```bash
  $ git clone https://github.com/stlink-org/stlink.git
  $ cd stlink
  $ mkdir out && cd out
  $ cmake ../ && make
  $ sudo make install
  ```


  ### Check the Nucleo-64 board status

  ```bash
  $ sudo st-info --probe
      Failed to parse flash type or unrecognized flash type

    detected chip_id parametres

    # Device Type: STM32F1xx_MD
    # Reference Manual: RM0008
    #
    chip_id 0x410
    flash_type 1
    flash_size_reg 0x1ffff7e0
    flash_pagesize 0x400
    sram_size 0x5000
    bootrom_base 0x1ffff000
    bootrom_size 0x800
    option_base 0x1ffff800
    option_size 0x10
    flags 2

    Found 1 stlink programmers
      version:    V2J39S27
      serial:     066FFF504857788667225719
      flash:      131072 (pagesize: 1024)
      sram:       20480
      chipid:     0x410

    detected chip_id parametres

    # Device Type: STM32F1xx_MD
    # Reference Manual: RM0008
    #
    chip_id 0x410
    flash_type 1
    flash_size_reg 0x1ffff7e0
    flash_pagesize 0x400
    sram_size 0x5000
    bootrom_base 0x1ffff000
    bootrom_size 0x800
    option_base 0x1ffff800
    option_size 0x10
    flags 2

      dev-type:   STM32F1xx_MD
  ```


  ### Read the binary from the Nucleo-64

  ```bash
  $ sudo st-flash read dump_from_nucleo.bin 0x8000000 128k
  $ sudo -E hexdump -C dump_from_nucleo.bin | vi -
  ```


  ### Write the binary to the Nucleo-64

  ```bash
  $ cd freeRTOS_tutorials/STM32freeRTOS-student/01_TASKMAN/Debug
  $ make all
  $ arm-none-eabi-objcopy -O binary 01_TASKMAN.elf 01_TASKMAN.bin
  $ sudo st-flash --debug write 01_TASKMAN.bin
  ```


## How to check the log or shell

  * Can connect the board through the interface of the ***/dev/ttyACMx***

  ```bash
  $ sudo minicom -D /dev/ttyACM0 -c on
  ```


## For debugging using the GDB

  * Install the prerequisite the packages

  ```bash
  $ sudo apt update
  $ sudp apt install gcc-arm-linux-gnueabi g++-arm-linux-gnueabi # for 32bit
  $ sudo apt install gcc-aarch64-linux-gnu g++-aarch64-linux-gnu # for 64bit
  $ sudo apt install gdb-multiarch libc6-dev-armhf-cross
  ```

  * Start the GDB server using the st-util

  ```bash
  $ st-util -p 1524
      st-util 1.7.0-186-gc4762e6
    Failed to parse flash type or unrecognized flash type

    detected chip_id parametres

    # Device Type: STM32F1xx_MD
    # Reference Manual: RM0008
    #
    chip_id 0x410
    flash_type 1
    flash_size_reg 0x1ffff7e0
    flash_pagesize 0x400
    sram_size 0x5000
    bootrom_base 0x1ffff000
    bootrom_size 0x800
    option_base 0x1ffff800
    option_size 0x10
    flags 2

    2022-03-19T23:33:19 INFO common.c: STM32F1xx_MD: 20 KiB SRAM, 128 KiB flash in at least 1 KiB pages.
    2022-03-19T23:33:19 INFO gdb-server.c: Listening at *:1524...
  ```

  * Open the new terminal, start the GDB

  ```bash
  $ arm-none-eabi-gdb 01_TASKMAN.elf
  (gdb) set arch arm
  (gdb) target remote localhost:1524
  (gdb) load 01_TASKMAN.elf
  (gdb) continue
  ```

  * Send the signal or occured the error

  ```bash
  (gdb) bt
  (gdb) info all-register
  (gdb) ...
  ```
