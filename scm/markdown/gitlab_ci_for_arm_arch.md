# How to setup the gitlab CI

## How to setup the Host server
* I use the host server as the Raspberry Pi3
* The DDNS name is the jslee24x.iptime.org and setup the port-forward for the sshd
* To execute the Gitlab-Runner as the Docker needs to install the Docker in the host
```bash
$ sudo apt update
$ sudo curl -fsSL https://get.docker.com/ | sudo sh
$ sudo usermod -aG docker $USER
```

## How to write the .gitlab-ci.yml file
* Please refer to the link below
	- <https://docs.gitlab.com/ee/ci/yaml/README.html>

## How to setup the gitlab runner on the host
* Please refer to the link below
	- <https://docs.gitlab.com/runner/install/linux-manually.html>
	- <https://www.devils-heaven.com/gitlab-runner-finally-use-your-raspberry-pi/>

* **Attention!!**
	* The installation method varie depending on the architecture(x86,ARM)
	* The *pycounter* project use the **Raspberry Pi 3 Model B Plus Rev 1.3**

* gitlab-runner install command,
```bash
$ curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
$ sudo apt install gitlab-runner
```

* Start the *gitlab-runner* on the Raspberry Pi
```bash
jerrylee@raspberrypi:~$ sudo gitlab-runner register
Runtime platform    arch=arm os=linux pid=5948 revision=4c96e5ad version=12.9.0
Running in system-mode.

Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com/):
https://gitlab.com/
Please enter the gitlab-ci token for this runner:
KeVnfRwBxrvqmyJ1JfvZ
Please enter the gitlab-ci description for this runner:
[raspberrypi]: gitlab-runner for the pycounter
Please enter the gitlab-ci tags for this runner (comma separated):
pycounter
Registering runner... succeeded                     runner=KeVnfRwB
Please enter the executor: custom, docker, docker-ssh, parallels, kubernetes, shell, ssh, virtualbox, docker+machine, docker-ssh+machine:
docker
Please enter the default Docker image (e.g. ruby:2.6):
pycounter:ci
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
```

* Please refer to find the \<My-Token\> as below,
![gitlab_runner_1](https://user-images.githubusercontent.com/54479819/78148837-44f09c80-7470-11ea-871e-851b9e7886df.png)

* After register the *gitlab-runner*, you can see below image,
![gitlab_runner_3](https://user-images.githubusercontent.com/54479819/78451943-77de9e80-76c3-11ea-8863-408d3229ebf3.png)

	* In the Raspberry Pi,
	```bash
	$ sudo gitlab-runner status
	Runtime platform	arch=arm os=linux pid=5993 revision=4c96e5ad version=12.9.0
	gitlab-runner: Service is running!
	```
	* Also, can check the gitlab-runner configuration file in the Raspberry,
	```bash
	$ sudo cat /etc/gitlab-runner/config.toml
	concurrent = 1
	check_interval = 0

	[session_server]
	  session_timeout = 1800

	[[runners]]
	  name = "gitlab-runner for the pycounter"
	  url = "https://gitlab.com/"
	  token = "Des1Mvzxnz-S8qLRJLNW"
	  executor = "docker"
	  [runners.custom_build_dir]
	  [runners.cache]
		[runners.cache.s3]
		[runners.cache.gcs]
	  [runners.docker]
		tls_verify = false
		image = "pycounter:ci"
		privileged = false
		disable_entrypoint_overwrite = false
		oom_kill_disable = false
		disable_cache = true
		pull_policy = "if-not-present"
		volumes = ["/cache"]
		shm_size = 0
	```
