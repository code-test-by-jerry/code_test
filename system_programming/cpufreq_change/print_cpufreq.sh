#!/bin/bash

while true
do
	cat /sys/devices/system/cpu/cpufreq/policy0/scaling_cur_freq
	cat /sys/devices/system/cpu/cpufreq/policy1/scaling_cur_freq
	cat /sys/devices/system/cpu/cpufreq/policy2/scaling_cur_freq
	cat /sys/devices/system/cpu/cpufreq/policy3/scaling_cur_freq
	echo
	sleep 1
done
