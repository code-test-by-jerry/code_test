#!/usr/bin/python3
""" Change the cpu frequency when running time """

import random
import threading
import time

import pyssh


class CPUFreq():
    """ for CPU frequency changing class"""

    __CPU_CORE_NUM = 4
    __DELAY_TIME = 5
    __MULTIPLE_CLOCK = 100000

    __CPU_PATH = "/sys/devices/system/cpu"
    __PSTATE_PATH = __CPU_PATH + "/intel_pstate/status"
    __POLICY_PATH = __CPU_PATH + "/cpufreq/policy"

    def __init__(self):
        self.__ssh = pyssh.SSHWrapper("192.168.2.3", 22, "root", "")

        self.__thread = None
        self.__thread_stop_flag = None

    def __del__(self):
        del self.__ssh

    ###########################################################################
    # external method
    def intel_pstate_status(self) -> str:
        """ get the intel_pstate """
        _, stdout, _ = self.__ssh.transmit_command("cat " +
                                                   CPUFreq.__CPU_PATH +
                                                   "/intel_pstate/status")

        ret = stdout.read().decode('ascii')
        if "active" in ret:
            return "active"
        elif "passive" in ret:
            return "passive"

        return "off"

    def intel_pstate_active(self):
        """ set the intel_pstate to "active" status """
        self.__set_intel_pstate("active")

    def intel_pstate_passive(self):
        """ set the intel_pstate to "passive" status """
        self.__set_intel_pstate("passive")

    def start_cpufreq_change(self) -> None:
        """ Start thread to change cpu frequency """

        # 4 cpu core
        for idx in range(0, CPUFreq.__CPU_CORE_NUM):
            # check to use the "userspace" governor
            command = "cat " + CPUFreq.__POLICY_PATH + str(idx) \
                + "/scaling_available_governors"
            _, stdout, _ = self.__ssh.transmit_command(command)

            if "userspace" not in stdout.read().decode('ascii'):
                print("[ERR] Can not use the \"userspace\" governor.")
                raise ValueError

            # set to the "userspace" governor
            governor_path = CPUFreq.__POLICY_PATH + str(idx) \
                + "/scaling_governor"
            self.__ssh.transmit_command("echo userspace > " + governor_path)

            _, stdout, _ = self.__ssh.transmit_command("cat " + governor_path)
            if "userspace" not in stdout.read().decode('ascii'):
                print("[ERR] Failed to set the \"userspace\" governor.")
                raise ValueError

        self.__thread = threading.Thread(target=self.__change_cpu_frequency)
        self.__thread.start()

    def stop_cpufreq_change(self) -> None:
        """ Stop thread to change cpu frequency """

        self.__thread_stop_flag = True
        if self.__thread:
            self.__thread.join()
            self.__thread = None

    ###########################################################################
    # internal method
    def __set_intel_pstate(self, set_status: str):
        if self.intel_pstate_status() != set_status:
            command = "echo " + set_status + " > " \
                + CPUFreq.__CPU_PATH + "/intel_pstate/status"

            # transmit command
            self.__ssh.transmit_command(command)

            if self.intel_pstate_status() != set_status:
                print("[ERR] Failed to set %s" % set_status)
                raise ValueError

    def __change_cpu_frequency(self):
        """ Thread which changing the cpu freqeuncy on the MRB """
        # err
        if self.__thread_stop_flag is False:
            print("[CPU Freq] Already thread running.")
            return

        # get min freq
        command = "cat " + CPUFreq.__POLICY_PATH + "0/scaling_min_freq"
        _, stdout, _ = self.__ssh.transmit_command(command)
        min_freq = \
            int(stdout.read().decode('ascii')) / CPUFreq.__MULTIPLE_CLOCK

        # get max freq
        command = "cat " + CPUFreq.__POLICY_PATH + "0/scaling_max_freq"
        _, stdout, _ = self.__ssh.transmit_command(command)
        max_freq = \
            int(stdout.read().decode('ascii')) / CPUFreq.__MULTIPLE_CLOCK

        # start
        self.__thread_stop_flag = False
        print("[CPU Freq] Thread Start!")

        while True:
            if self.__thread_stop_flag is True:
                break

            freq = random.randrange(min_freq, max_freq)
            command = "echo " + str(freq * CPUFreq.__MULTIPLE_CLOCK) \
                + " > " + CPUFreq.__POLICY_PATH + "0/scaling_setspeed"

            time.sleep(CPUFreq.__DELAY_TIME)

            self.__ssh.transmit_command(command)
            print(command)


if __name__ == '__main__':
    CPU = CPUFreq()

    CPU.intel_pstate_active()

    CPU.intel_pstate_passive()

    CPU.start_cpufreq_change()

    CNT = 0
    while CNT < 600:
        CNT += 1
        time.sleep(1)

    CPU.stop_cpufreq_change()
