#!/usr/bin/python3
# pylint: disable=import-error, too-few-public-methods
"""connect the ssh using the paramiko"""

import time

import paramiko


class SSHWrapper():
    """for ssh class"""

    __RISK_TEMP = 40
    __DELAY_TIME = 10

    def __init__(self,
                 hostname: str,
                 port: int = 22,
                 username: str = "root",
                 password: str = ""):
        # connect
        self.__ssh = paramiko.SSHClient()
        self.__ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.__ssh.connect(hostname, port, username, password)

    def __del__(self):
        self.__ssh.close()
        del self.__ssh

    ###########################################################################
    # external method
    def transmit_command(self, command: str) -> tuple:
        """ tranmit command thorugh ssh to the MRB """
        return self.__ssh.exec_command(command)

    ###########################################################################
    # internal method


if __name__ == '__main__':
    # connect to the MRB
    SSH_DEV = SSHWrapper("192.168.2.3", 22, "root", "")

    CNT = 0
    while CNT < 60:
        print("Tic Tok")
        CNT += 1
        time.sleep(1)

    print("SSH test done.")
