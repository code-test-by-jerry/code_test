#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/fs.h>
#include <linux/kdev_t.h>
#include <linux/module.h>

#ifdef pr_fmt
#undef pr_fmt
#define pr_fmt(fmt) "%s:" fmt, __func__
#endif

static int _pcd_open(struct inode *inode, struct file *filp);
static int _pcd_release(struct inode *inode, struct file *filp);
static ssize_t _pcd_read(struct file *filp, char __user *buff, size_t count,
                         loff_t *f_pos);
static ssize_t _pcd_write(struct file *filp, const char __user *buff,
                          size_t count, loff_t *f_pos);
static loff_t _pcd_lseek(struct file *filp, loff_t off, int whence);

char dev_buf[512];

/* This holds the device number */
dev_t dev_num;

struct cdev pcd_cdev;
struct file_operations pcd_fops = {.open = _pcd_open,
                                   .release = _pcd_release,
                                   .read = _pcd_read,
                                   .write = _pcd_write,
                                   .llseek = _pcd_lseek,
                                   .owner = THIS_MODULE};
struct class *class_pcd;
struct device *device_pcd;

static int __init _pseudo_driver_init(void)
{
    /* Dynamically allocate a device number */
    alloc_chrdev_region(&dev_num, 0, 1, "pcd_deivces");
    pr_info("dev num <%d>:<%d>\n", MAJOR(dev_num), MINOR(dev_num));

    /* Initialize the cdev structure with fops */
    cdev_init(&pcd_cdev, &pcd_fops);
    pcd_cdev.owner = THIS_MODULE;

    /* Register a device(cdev struct) with VFS */
    cdev_add(&pcd_cdev, dev_num, 1);

    /* create device class under /sys/class */
    class_pcd = class_create(THIS_MODULE, "pcd_class");
    if (class_pcd == NULL)
    {
    }

    /* populate the sysfs with device information */
    device_pcd = device_create(class_pcd, NULL, dev_num, NULL, "pcd");
    if (device_pcd == NULL)
    {
    }

    pr_info("Success the module registeration!");

    return 0;
}

static void __exit _pseudo_driver_cleanup(void)
{
    device_destroy(class_pcd, dev_num);
    class_destroy(class_pcd);
    cdev_del(&pcd_cdev);
    unregister_chrdev_region(dev_num, 1);
    pr_info("module unloaded!\n");
}

static int _pcd_open(struct inode *inode, struct file *filp)
{
    pr_info("open!\n");
    return 0;
}

static int _pcd_release(struct inode *inode, struct file *filp)
{
    pr_info("close!\n");
    return 0;
}

static ssize_t _pcd_read(struct file *filp, char __user *buff, size_t count,
                         loff_t *f_pos)
{
    pr_info("read requested for %zu bytes\n", count);
    return 0;
}

static ssize_t _pcd_write(struct file *filp, const char __user *buff,
                          size_t count, loff_t *f_pos)
{
    pr_info("write requested for %zu bytes\n", count);
    return 0;
}

static loff_t _pcd_lseek(struct file *filp, loff_t off, int whence)
{
    pr_info("lseek!\n");
    return 0;
}

module_init(_pseudo_driver_init);
module_exit(_pseudo_driver_cleanup);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Lee, Jerry J");
MODULE_DESCRIPTION("A simple pseudo char driver module");
MODULE_INFO(board, "Beaglebone black REV A5");
