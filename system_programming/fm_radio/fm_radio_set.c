#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/time.h>

#include <errno.h>

#define BOARD_TEST
#define I2C_DEV_PATH "/dev/i2c-%d"

#ifdef BOARD_TEST
#define FREQ_TABLE_FILE "/data/freq_table.txt"
#else
#define FREQ_TABLE_FILE "./freq_table.txt"
#endif

#define MAX_SMBUS_BLOCK_SIZE 32
#define DEF_I2C_SLAVE_FORCE 0x0706
#define DEF_I2C_RDWR 0x0707
#define DEF_I2C_SMBUS 0x0720

#define SMBUS_BLOCK_DATA 5
#define I2C_SMBUS_BLOCK_DATA 8

#define DEF_I2C_M_RD 0x0001

#define DIRANA_I2C_BUS 4
#define DIRANA_I2C_SLAVE_ADDR 0x1c //(0x38 >> 1)

//#define PAEKR_DEBUG

#ifdef PAEKR_DEBUG
#define DEBUG_MSG(fmt, ...)                                                    \
    do                                                                         \
    {                                                                          \
        printf("[DEBUG] " fmt "\n", ##__VA_ARGS__);                            \
    } while (0)
#else
#define DEBUG_MSG(...)
#endif

#define ERR_MSG(fmt, ...)                                                      \
    do                                                                         \
    {                                                                          \
        printf("[ERR] " fmt " / func : %s, line : %d\n", ##__VA_ARGS__,        \
               __func__, __LINE__);                                            \
    } while (0)

union i2c_smbus_data {
    uint8_t byte;
    uint16_t word;
    uint8_t block[MAX_SMBUS_BLOCK_SIZE + 2];
};

struct i2c_smbus_ioctl_data
{
    char read_write;
    uint8_t command;
    int size;
    union i2c_smbus_data *data;
};

struct i2c_msg
{
    uint16_t addr;
    uint16_t flags;
    uint16_t len;
    uint8_t *buf;
};

struct i2c_rdwr_ioctl_data
{
    struct i2c_msg *msgs;
    uint32_t nmsgs;
};

/*
 *  The struct radio_information have radio informations.
 *
 *	freq					: frequency, The unit is KHz.
 *	output					: signal strength, The unit is
 *KW. pbroadcasting_station	: Broadcasting Station
 *	pcall_sign				: Call Sign style
 */
typedef struct radio_information
{
    uint16_t freq;   // kHz
    uint16_t output; // kW
    char *pbroadcasting_station;
    char *pcall_sign;

} radio_info_t;

static void *_fm_radio_memory_allocate(size_t size);
static int _fm_radio_memory_free(void *del);

static int _fm_radio_dirana_write_regitster(char bus, uint16_t addr,
                                            uint8_t reg, int size,
                                            uint8_t *write_data);
static int _fm_radio_dirana_read_register(char bus, uint16_t addr, uint8_t reg,
                                          int w_size, int r_size,
                                          uint8_t *write_data, uint8_t *result);

static int _fm_radio_get_frequency(uint16_t *pfreq);
static int _fm_radio_set_frequency(int size, char *pselect,
                                   const radio_info_t *pradio);
static int _fm_radio_print_to_select_menu(int size, uint16_t freq,
                                          char *pselect, size_t buf_size,
                                          const radio_info_t *pradio);

static radio_info_t *_fm_radio_set_freq_table(int *size);
static int _fm_radio_clear_freq_table(radio_info_t **radio, int size);
static int _fm_radio_select_freq_menu(const radio_info_t *radio, int size);

static int _fm_radio_write(int size, uint8_t reg, const char **argv);
static int _fm_radio_read(int w_size, int r_size, uint8_t reg,
                          const char **argv);

static void *_fm_radio_memory_allocate(size_t size)
{
    uint8_t *pnew = calloc(1, size * sizeof(uint8_t));
    if (pnew == NULL)
        ERR_MSG("Failed to allocate memory.");

    return pnew;
}

static int _fm_radio_memory_free(void *del)
{
    if (del == NULL)
    {
        ERR_MSG("Invalid parameters.");
        return -EPERM;
    }

    free(del);
    return 0;
}

static int _fm_radio_dirana_write_regitster(char bus, uint16_t addr,
                                            uint8_t reg, int size,
                                            uint8_t *write_data)
{
    struct i2c_smbus_ioctl_data ioctl_data;
    union i2c_smbus_data data;

    /* set bus channel */
    char i2c_dev[15];
    snprintf(i2c_dev, sizeof(i2c_dev), I2C_DEV_PATH, bus);

#ifdef BOARD_TEST
    /* open i2c path */
    int fd = open(i2c_dev, O_RDWR | O_SYNC);
    if (fd < 0)
    {
        ERR_MSG("Failed to open. %s", i2c_dev);
        return -EPERM;
    }

    /* send slave address */
    int ret = ioctl(fd, DEF_I2C_SLAVE_FORCE, addr);
    if (ret < 0)
    {
        ERR_MSG("Failed to open through i2c slave address.");
        goto error;
    }
#endif

    /* data have write data size + real writing data */
    data.block[0] = size;
    memcpy(&data.block[1], write_data, size);

    ioctl_data.read_write = 0;
    ioctl_data.command = reg;
    ioctl_data.size = I2C_SMBUS_BLOCK_DATA;
    ioctl_data.data = &data;

    DEBUG_MSG("----------- Write Info -----------");
    DEBUG_MSG("Bus : %s", i2c_dev);
    DEBUG_MSG("Addr : 0x%02x", addr);
    DEBUG_MSG("Reg : 0x%02x", reg);

    int i;
    for (i = 0; i < size; i++)
        DEBUG_MSG("Reg : 0x%02x / Write data : 0x%02x", reg + i,
                  data.block[i + 1]);

#ifdef BOARD_TEST
    /* send to write date */
    ret = ioctl(fd, DEF_I2C_SMBUS, &ioctl_data);
    if (ret < 0)
    {
        ERR_MSG("Failed to write through smbus to i2c.");
        goto error;
    }

    close(fd);
#endif

    return 0;

#ifdef BOARD_TEST
error:
    close(fd);
    return -EPERM;
#endif
}

static int _fm_radio_dirana_read_register(char bus, uint16_t addr, uint8_t reg,
                                          int w_size, int r_size,
                                          uint8_t *write_data, uint8_t *result)
{
    struct i2c_rdwr_ioctl_data rw_data;
    struct i2c_msg msg[2];

    /* set bus channel */
    char i2c_dev[15];
    snprintf(i2c_dev, sizeof(i2c_dev), I2C_DEV_PATH, bus);

#ifdef BOARD_TEST
    /* open i2c path */
    int fd = open(i2c_dev, O_RDWR | O_SYNC);
    if (fd < 0)
    {
        ERR_MSG("Failed to open. %s", i2c_dev);
        return -EPERM;
    }

    /* send slave address */
    int ret = ioctl(fd, DEF_I2C_SLAVE_FORCE, addr);
    if (ret < 0)
    {
        ERR_MSG("Failed to open through i2c slave address.");
        goto error;
    }
#endif

    /*
     * The dirana chip needs to send address 0xe0 to read register
     * from 0x00 to 0x4f and 0x60 to 0x7f only.
     */
    int buf_size;
    uint8_t buf[2];
    if (reg <= 0x4f || (reg >= 0x60 && reg <= 0x7f))
    {
        buf_size = 2;
        buf[0] = 0xe0;
        buf[1] = reg;
    }
    else
    {
        buf_size = w_size + 1;
        buf[0] = reg;

        int i;
        for (i = 0; i < w_size; i++)
        {
            buf[i + 1] = write_data[i];
            printf("@@ 0x%02x\n", buf[i + 1]);
        }
    }

    /* for writing data */
    msg[0].addr = addr;
    msg[0].len = buf_size;
    msg[0].flags = 0;
    msg[0].buf = buf;

    /* for reading data */
    msg[1].addr = addr;
    msg[1].len = r_size;
    msg[1].flags = DEF_I2C_M_RD;
    msg[1].buf = result;

    rw_data.nmsgs = 2;
    rw_data.msgs = msg;

#ifdef BOARD_TEST
    /* send to write and read data */
    ret = ioctl(fd, DEF_I2C_RDWR, &rw_data);
    if (ret < 0)
    {
        ERR_MSG("Failed to read/write to I2C.");
        goto error;
    }

    DEBUG_MSG("read data : 0x%02x", *result);

    close(fd);
#endif
    return 0;

#ifdef BOARD_TEST
error:
    close(fd);
    return -EPERM;
#endif
}

static int _fm_radio_get_frequency(uint16_t *pfreq)
{
    if (pfreq == NULL)
    {
        ERR_MSG("Invalid parameters.");
        return -EPERM;
    }

    /* Freq Read from register 0x01, 0x02 */
    uint8_t read_buf = 0x0;
    int ret = _fm_radio_dirana_read_register(
        DIRANA_I2C_BUS, DIRANA_I2C_SLAVE_ADDR, 0x1, 0, 1, NULL, &read_buf);
    if (ret < 0)
        return -EPERM;

    *pfreq = read_buf;
    *pfreq <<= 8;

    ret = _fm_radio_dirana_read_register(DIRANA_I2C_BUS, DIRANA_I2C_SLAVE_ADDR,
                                         0x2, 0, 1, NULL, &read_buf);
    if (ret < 0)
        return -EPERM;

    *pfreq |= read_buf;

    printf("Success to get frequency : %.1lf\n", (double)*pfreq * 0.01);

    return 0;
}

static int _fm_radio_set_frequency(int size, char *pselect,
                                   const radio_info_t *pradio)
{
    if (pselect == NULL)
    {
        ERR_MSG("Invalid parameters.");
        return -EPERM;
    }

    int ret = 0;
    uint8_t write_buf[3] = {
        0x10,
    };

    if (pselect[0] == 'c')
    {
        /* custom setting */
        char want_freq[8] = {
            0,
        };
        printf("\nInput you want to change the frequency value\n"
               "ex) 91.9MHz -> input \"9190\"\n"
               "Frequency : ");
        fgets(want_freq, sizeof(want_freq), stdin);
        want_freq[strlen(want_freq) - 1] = '\0';

        /* set write buffer */
        uint16_t freq = (uint16_t)atoi(want_freq);
        write_buf[1] = (freq >> 8) & 0xff;
        write_buf[2] = freq & 0xff;
    }
    else
    {
        /* setting using index in menu */
        int index = atoi(pselect);
        if (index < 1 || index > size - 1)
        {
            ERR_MSG("Error to input index value.");
            return -EPERM;
        }

        /* set write buffer */
        write_buf[1] = (pradio[index - 1].freq >> 8) & 0xff;
        write_buf[2] = pradio[index - 1].freq & 0xff;
    }

    /* write to register to change frequency */
    ret = _fm_radio_dirana_write_regitster(
        DIRANA_I2C_BUS, DIRANA_I2C_SLAVE_ADDR, 0x00, 3, write_buf);
    if (ret < 0)
        return -EPERM;

    return 0;
}

static int _fm_radio_print_to_select_menu(int size, uint16_t freq,
                                          char *pselect, size_t buf_size,
                                          const radio_info_t *pradio)
{
    int i;

    /* find current broadcasting */
    for (i = 0; i < size - 1; i++)
    {
        if (freq == pradio[i].freq)
            break;
    }

    /* print current broadcasting */
    printf("\n------------------------------------------------------------\n");
    printf("Current Broadcasting : [%2d] %.1lfMHz %s\n", i + 1,
           (double)freq * 0.01, pradio[i].pbroadcasting_station);
    printf("------------------------------------------------------------\n\n");

    /* for select */
    for (i = 0; i < size - 1; i++)
        printf("[%2d] Freq : %.1lfMHz\tBCS : %s\n", i + 1,
               (double)pradio[i].freq * 0.01, pradio[i].pbroadcasting_station);
    printf("\n[c] Can input you want frequency.\n");

    printf("\nSelect index what you want channel number.\n");
    printf("If you want to exit this program, you input 'q'.\n");
    printf("Channel Number : ");
    fgets(pselect, buf_size, stdin);
    pselect[strlen(pselect) - 1] = '\0';

    return 0;
}

static radio_info_t *_fm_radio_set_freq_table(int *size)
{
    if (size == NULL)
    {
        ERR_MSG("Invalid parameters.");
        return NULL;
    }

    /* new radio information */
    radio_info_t *radio = NULL;

    /* load freq information from FREQ_TABLE_FILE */
    FILE *file = fopen(FREQ_TABLE_FILE, "r");
    if (file == NULL)
    {
        ERR_MSG("Failed to open %s", FREQ_TABLE_FILE);
        return NULL;
    }

    int cnt;
    int real_size = 0;
    char buf[128];
    while (fgets(buf, sizeof(buf), file))
    {
        /* filter about blank or start # */
        buf[strlen(buf) - 1] = '\0';
        if (buf[0] == '\0' || buf[0] == ' ' || buf[0] == '#')
            continue;
        /*
         *	The radio is radio information array.
         *	For example, in the broadcast, the name is "KBS the 2nd radio"
         *and frequency is 89.1KHz, signal strength is 10KW and call sign is
         *"HLCK-FM", write as below.
         *
         *	8910,10,KBS the 2nd radio,HLCK-FM
         */

        /* memory allocation for radio info */
        radio = realloc(radio, sizeof(radio_info_t) * (real_size + 1));
        if (radio == NULL)
        {
            ERR_MSG("Failed to allocate memory.");
            goto error;
        }
        *size = ++real_size;

        /* real information */
        cnt = 0;
        char *ptr = strtok(buf, ",");
        while (ptr)
        {
            switch (cnt)
            {
            case 0:
                radio[real_size - 1].freq = (uint16_t)atoi(ptr);
                break;
            case 1:
                radio[real_size - 1].output = (uint16_t)atoi(ptr);
                break;
            case 2:
                radio[real_size - 1].pbroadcasting_station = strdup(ptr);
                if (radio[real_size - 1].pbroadcasting_station == NULL)
                {
                    ERR_MSG("Failed to allocate memory.");
                    goto error;
                }
                break;
            case 3:
                radio[real_size - 1].pcall_sign = strdup(ptr);
                if (radio[real_size - 1].pcall_sign == NULL)
                {
                    ERR_MSG("Failed to allocate memory.");
                    goto error;
                }
                break;
            default:
                break;
            }
            ptr = strtok(NULL, ",");
            cnt++;
        }
    }
    fclose(file);

    return radio;

error:
    _fm_radio_clear_freq_table(&radio, *size);
    return NULL;
}

static int _fm_radio_clear_freq_table(radio_info_t **radio, int size)
{
    /* pointer error */
    if (radio == NULL || *radio == NULL)
    {
        ERR_MSG("Invalid parameters.");
        return -EPERM;
    }

    int i;
    for (i = 0; i < size; i++)
    {
        if (*radio + i)
        {
            if ((*radio)[i].pbroadcasting_station)
            {
                free((*radio)[i].pbroadcasting_station);
                (*radio)[i].pbroadcasting_station = NULL;
            }

            if ((*radio)[i].pcall_sign)
            {
                free((*radio)[i].pcall_sign);
                (*radio)[i].pcall_sign = NULL;
            }
        }
    }

    free(*radio);
    *radio = NULL;

    return 0;
}

static int _fm_radio_select_freq_menu(const radio_info_t *radio, int size)
{
    while (1)
    {
        /* get current frequency */
        uint16_t freq = 0x00;
        int ret = _fm_radio_get_frequency(&freq);
        if (ret < 0)
            return -EPERM;

        /* menu */
        char select[8] = {
            0,
        };
        _fm_radio_print_to_select_menu(size, freq, select, sizeof(select),
                                       radio);
        if (select[0] == 'q')
            break;

        /* set to changing frequency */
        ret = _fm_radio_set_frequency(size, select, radio);
        if (ret < 0)
            return -EPERM;
    }

    return 0;
}

static int _fm_radio_write(int size, uint8_t reg, const char **argv)
{
    if (size <= 0)
    {
        ERR_MSG("Invalid parameters.");
        return -EPERM;
    }

    /* allocated memory likes size for writing buffer */
    uint8_t *write_buf =
        (uint8_t *)_fm_radio_memory_allocate(sizeof(uint8_t) * size);
    if (write_buf == NULL)
        return -EPERM;

    /* write data input to buffer */
    int i;
    for (i = 0; i < size; i++)
        write_buf[i] = strtoul(argv[i], NULL, 16);

    int ret = _fm_radio_dirana_write_regitster(
        DIRANA_I2C_BUS, DIRANA_I2C_SLAVE_ADDR, reg, size, write_buf);

    /* destroy to allocated memory */
    _fm_radio_memory_free(write_buf);

    return ret;
}

static int _fm_radio_read(int w_size, int r_size, uint8_t reg,
                          const char **argv)
{
    if (r_size <= 0)
    {
        ERR_MSG("Invalid parameters.");
        return -EPERM;
    }

    uint8_t *write_buf = NULL;
    uint8_t *read_buf = NULL;

    if (w_size > 0)
    {
        /* allocated memory likes size for writing buffer */
        write_buf =
            (uint8_t *)_fm_radio_memory_allocate(sizeof(uint8_t) * w_size);
        if (write_buf == NULL)
            return -EPERM;

        /* write data input to buffer */
        int i;
        for (i = 0; i < w_size; i++)
            write_buf[i] = strtoul(argv[i], NULL, 16);
    }

    /* allocated memory likes size for reading buffer */
    read_buf = (uint8_t *)_fm_radio_memory_allocate(sizeof(uint8_t) * r_size);
    if (read_buf == NULL)
        goto error;

    int ret = _fm_radio_dirana_read_register(DIRANA_I2C_BUS,
                                             DIRANA_I2C_SLAVE_ADDR, reg, w_size,
                                             r_size, write_buf, read_buf);
    if (ret < 0)
        goto error;

    int i;
    for (i = 0; i < r_size; i++)
        printf("reg: 0x%02x / read data : 0x%02x\n", reg + i, read_buf[i]);

    /* destroy to allocated memory */
    if (write_buf)
        _fm_radio_memory_free(write_buf);

    _fm_radio_memory_free(read_buf);
    return 0;

error:
    _fm_radio_memory_free(write_buf);
    _fm_radio_memory_free(read_buf);
    return -EPERM;
}

int main(int argc, char *argv[])
{
    int ret = 0;
    if (argc < 3)
    {
        int size = 0;

        /* set frequency table from freq_table.txt */
        radio_info_t *radio = _fm_radio_set_freq_table(&size);
        if (radio == NULL)
            return -EPERM;

        /* doing menu */
        _fm_radio_select_freq_menu(radio, size);

        /* destroy allocated memory for frequency table */
        ret = _fm_radio_clear_freq_table(&radio, size);
        if (ret < 0)
            return -EPERM;
    }
    else
    {
#if 0
				app name	r/w		reg addr	write data
		Write : ./test		w		0x00		0x10 0x23 0xe6
		Read  : ./test		r		0x00
#endif
        uint8_t reg = (uint8_t)strtoul(argv[2], NULL, 16);
        if (!strcmp(argv[1], "w"))
        {
            /* write */
            ret = _fm_radio_write(argc - 3, reg, (const char **)&argv[3]);
            if (ret < 0)
                return -EPERM;
        }
        else if (!strcmp(argv[1], "r"))
        {
            /* read */
            int w_size = argc - 3;
            int r_size = 1;
            char **param_argv = w_size > 0 ? argv : NULL;

            ret = _fm_radio_read(w_size, r_size, reg,
                                 (const char **)&param_argv[3]);
            if (ret < 0)
                return -EPERM;
        }
        else
        {
            /* error */
            ERR_MSG("Parameters error!!");
            return -EPERM;
        }
    }

    return 0;
}
