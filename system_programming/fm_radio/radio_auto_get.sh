#!/bin/bash

###############################################################################
#	This script file operates automatically radio recording during 60 sec	  #
#	with to change frequency each 10 sec using fm_radio_set binary.			  #
###############################################################################

BACK_CR="\033[0m"
RED_CR="\033[0;31m"
GREEN_CR="\033[0;32m"

RECORD_NAME="record_radio.pcm"
USER_APP="fm_radio_set"
FREQ_TABLE='freq_table.txt'

# for color echo
echo_func()
{
	if [ ${2} -eq 1 ]; then
		echo -e ${RED_CR}${1}${BACK_CR}
	else
		echo -e ${GREEN_CR}${1}${BACK_CR}
	fi
}

# for checking ALSA and kill process
kill_alsa()
{
	alsa_pid=`adb shell pgrep -f alsa_aplay`
	if [ ! "${alsa_pid}" = ""  ]; then
		echo_func "[FM Radio] Exit about Alsa" 1
		adb shell kill -9 ${alsa_pid}
	fi
}

clear_func()
{
	kill_alsa

	# remove user application
	ret="$(adb shell ls /data/${USER_APP} > /dev/null 2>&1; echo $?)"
	if [ ${ret} -eq 0 ]; then
		echo_func "[FM Radio] Delete ${USER_APP} file on the MRB" 1
		adb shell rm /data/${USER_APP}
	fi

	# remove freq_table
	ret="$(adb shell ls /data/${FREQ_TABLE} > /dev/null 2>&1; echo $?)"
	if [ ${ret} -eq 0 ]; then
		echo_func "[FM Radio] Delete ${FREQ_TABLE} file on the MRB" 1
		adb shell rm /data/${FREQ_TABLE}
	fi
}
trap 'clear_func' 0 1 2 3 6 9 11 14 15

# To change FM frequency
change_freq()
{
	adb shell <<-EOF
		/data/${USER_APP}
		${1}
		q
	EOF
	sleep 10
}

###############################################################################

# checking to adb
adb devices | while read line
do
	if [ ! "${line}" = "" ] && \
		[ `echo ${line} | awk '{print $2}'` = "device" ]; then
		# adb root
		echo_func "[FM Radio] for permission \"adb root\"" 0
		adb root

		# push fm_radio_set and freq_table to MRB
		echo_func "[FM Radio] push binary file" 0
		adb push ${USER_APP} /data
		adb shell chmod u+x /data/${USER_APP}
		adb push ${FREQ_TABLE} /data
		adb shell chmod u+r /data/${FREQ_TABLE}

		# execute dirana_config.sh
		echo_func "[FM Radio] Start dirana chip configuration!!" 0
		adb shell /vendor/bin/dirana_config.sh
#
#		if [[ "$?" -ne "0" ]]; then
#			adb push dirana_config.sh /data
#			adb shell chmod u+x /data/dirana_config.sh
#			adb shell /data/dirana_config.sh
#			adb shell rm /data/dirana_config.sh
#		fi

		# execute alsa_aplay
		echo_func "[FM Radio] Recording start during 60sec!!" 0
		kill_alsa
		adb shell \
			alsa_aplay -C -t raw -c 2 -D hw:0,11 -f S24_LE -d 60 -r 48000 \
			/data/${RECORD_NAME} &
		sleep 1

		# change FM frequency each 10sec
		freq_idx=(1 3 4 8 10 22)
		for idx in ${freq_idx[*]}
		do
			echo_func "[FM Radio] Recording the channel number : ${idx}" 0
			change_freq ${idx} > /dev/null
		done

		# Get dump.wav file from MRB to localhost
		sleep 1
		echo
		echo_func "[FM Radio] Get ${RECORD_NAME} file,," 0
		adb pull /data/${RECORD_NAME} ./

		# Test Done
		sleep 1
		echo_func "[FM Radio] Done" 0
	fi
done
