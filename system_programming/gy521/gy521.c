/**
 * @file gy521.c
 * @brief user application about gy521 through i2c interface.
 * @author Lee, Jerry J(jerry.j.lee@intel.com)
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/time.h>

///////////////////////////////////////////////////////////////////////////////

#define MAX_BUF 1024

#define MAX_SMBUS_BLOCK_SIZE 32
#define DEF_I2C_SLAVE_FORCE 0x0706
#define DEF_I2C_RDWR 0x0707
#define DEF_I2C_SMBUS 0x0720

#define SMBUS_BLOCK_DATA 5
#define I2C_SMBUS_BLOCK_DATA 8

#define DEF_I2C_M_RD 0x0001

//#define I2C_NUM					3
#define GY521_ADDR (0xd0 >> 1)
#define GY521_GET_DATA_COUNT 14

//#define GPIO_CTRL
#define SYSFS_GPIO_DIR "/sys/class/gpio"

///////////////////////////////////////////////////////////////////////////////

#define PAEKR_DEBUG

#ifdef PAEKR_DEBUG
#define DEBUG_MSG(fmt, ...)                                                    \
    do                                                                         \
    {                                                                          \
        printf("[DEBUG] " fmt "\n", ##__VA_ARGS__);                            \
    } while (0)
#else
#define DEBUG_MSG(...)
#endif

#define ERR_MSG(fmt, ...)                                                      \
    do                                                                         \
    {                                                                          \
        printf("[ERR] " fmt " / func : %s, line : %d\n", ##__VA_ARGS__,        \
               __func__, __LINE__);                                            \
    } while (0)

///////////////////////////////////////////////////////////////////////////////

struct i2c_msg
{
    uint16_t addr;
    uint16_t flags;
    uint16_t len;
    uint8_t *buf;
};

struct i2c_rdwr_ioctl_data
{
    struct i2c_msg *msgs;
    uint32_t nmsgs;
};

typedef struct i2c_information
{
    int fd;
    int num;
    char path[MAX_BUF];

    uint16_t addr;
    uint8_t reg;

    int w_size;
    int r_size;

    uint8_t w_data[MAX_BUF];
    uint8_t r_data[MAX_BUF];

} i2c_t;

typedef struct gy521_information
{
    i2c_t *pi2c;

} gy521_t;

typedef enum
{
    CONFIG = 0x1a,
    ACCEL_CONFIG = 0x1c,

    MOT_THR = 0x1f,
    MOT_DUR = 0x20,
    INT_ENABLE = 0x38,

    PWR_MGMT_1 = 0x6b,
    PWR_MGMT_2

} GY521_REG_e;

typedef enum
{
    CYCLE = 5,
    SLEEP

} PWR_MGMT_1_e;

typedef enum
{
    STBY_ZA = 3,
    STBY_YA,
    STBY_XA,
    LP_WAKE_CTRL_0,
    LP_WAKE_CTRL_1

} PWR_MGMT_2_e;

enum
{
    MOT_EN = 6,
};

///////////////////////////////////////////////////////////////////////////////

static int _i2c_dev_open(i2c_t *pi2c);
static int _i2c_dev_close(i2c_t *pi2c);
static int _i2c_dev_ioctl(i2c_t *pi2c);

static int _i2c_write_one_byte(i2c_t *pi2c, uint8_t reg, uint8_t w_data);
static int _i2c_read_one_byte(i2c_t *pi2c, uint8_t reg);

static int _gy521_set_init(gy521_t *pgy);
static int _gy521_set_accel(gy521_t *pgy);
static int _gy521_set_motion_reg(gy521_t *pgy);
static int _gy521_set_freq_and_cycle(gy521_t *pgy);
#if 0
static int _gy521_gyro_scale(gy521_t *pgy);
static int _gy521_accerlator_scale(gy521_t *pgy);
static int _gy521_set_mot_int(gy521_t *pgy);
#endif

static int _gy521_get_data(gy521_t *pgy);
static int _gy521_data_print(gy521_t *pgy);

static int _gy521_init(gy521_t *pgy, int num);
static int _gy521_destory(gy521_t *pgy);

#ifdef GPIO_CTRL
static int _gpio_export(unsigned int gpio_num);
static int _gpio_unexport(unsigned int gpio_num);
static int _gpio_set_dir(unsigned int gpio_num);

static int _gpio_open(unsigned int gpio_num, int *pfd);
static int _gpio_close(int *pfd);

static int _gpio_get_val(unsigned int gpio_num);
#endif

///////////////////////////////////////////////////////////////////////////////

static int _i2c_dev_open(i2c_t *pi2c)
{
    if (pi2c == NULL)
    {
        ERR_MSG("Parameter's point NULL.");
        goto error;
    }

    /* create i2c path */
    char dir_buf[MAX_BUF];
    snprintf(dir_buf, sizeof(dir_buf), "/dev/i2c-%d", pi2c->num);

    /* open i2c */
    int fd = open(dir_buf, O_RDWR | O_SYNC);
    if (fd < 0)
    {
        ERR_MSG("Failed to open the directory : %s", dir_buf);
        goto error;
    }

    pi2c->fd = fd;
    memmove(pi2c->path, dir_buf, sizeof(dir_buf));

    DEBUG_MSG("fd : %d", pi2c->fd);
    DEBUG_MSG("path : %s", pi2c->path);

    return 0;

error:
    return -1;
}

static int _i2c_dev_close(i2c_t *pi2c)
{
    if (pi2c == NULL)
    {
        ERR_MSG("Parameter's point NULL.");
        goto error;
    }

    /* destory i2c path */
    if (pi2c->fd != -1)
    {
        close(pi2c->fd);
        pi2c->fd = -1;
    }

    return 0;

error:
    return -1;
}

static int _i2c_dev_ioctl(i2c_t *pi2c)
{
    if (pi2c == NULL)
    {
        ERR_MSG("Parameter's point NULL.");
        goto error;
    }

    /* send slave address */
    int ret = ioctl(pi2c->fd, DEF_I2C_SLAVE_FORCE, pi2c->addr);
    if (ret < 0)
    {
        ERR_MSG("Failed to open through i2c slave address.");
        goto error;
    }

    /* making message for i2c */
    struct i2c_rdwr_ioctl_data rw_data;
    struct i2c_msg msg[2];

    uint8_t *pw_buf = (uint8_t *)malloc(sizeof(uint8_t) * (pi2c->w_size + 1));
    if (pw_buf == NULL)
    {
        ERR_MSG("Failed to allocate memory for uint8_t.");
        goto error;
    }

    /* add register address in w_buf */
    pw_buf[0] = pi2c->reg;
    memmove(&pw_buf[1], pi2c->w_data, sizeof(uint8_t) * pi2c->w_size);

    /* set rw_data */
    rw_data.nmsgs = 1;
    rw_data.msgs = msg;

    /* for writing data */
    msg[0].addr = pi2c->addr;
    msg[0].len = pi2c->w_size + 1;
    msg[0].flags = 0;
    msg[0].buf = pw_buf;

    if (pi2c->r_size > 0)
    {
        rw_data.nmsgs++;

        /* for reading data */
        msg[1].addr = pi2c->addr;
        msg[1].len = pi2c->r_size;
        msg[1].flags = DEF_I2C_M_RD;
        msg[1].buf = pi2c->r_data;
    }

    DEBUG_MSG("----------- I2C Info -----------");
    DEBUG_MSG("Bus : %s", pi2c->path);
    DEBUG_MSG("Addr : 0x%02x", pi2c->addr);
    DEBUG_MSG("Reg : 0x%02x", pi2c->reg);

    int i;
    for (i = 0; i < pi2c->w_size; i++)
        DEBUG_MSG("Reg : 0x%02x / Write data : 0x%02x", pi2c->reg + i,
                  pi2c->w_data[i]);

    /* send to write data */
    if (ioctl(pi2c->fd, DEF_I2C_RDWR, &rw_data) < 0)
    {
        ERR_MSG("Failed to read/write to I2C.");
        goto error;
    }

    for (i = 0; i < pi2c->r_size; i++)
        DEBUG_MSG("Reg : 0x%02x / Read data : 0x%02x", pi2c->reg + i,
                  pi2c->r_data[i]);
    DEBUG_MSG("");

    free((void *)pw_buf);
    pw_buf = NULL;

    return 0;

error:
    return -1;
}

static int _i2c_write_one_byte(i2c_t *pi2c, uint8_t reg, uint8_t w_data)
{
    if (pi2c == NULL)
    {
        ERR_MSG("Parameters point NULL.");
        goto error;
    }

    /* set reg */
    pi2c->reg = reg;

    pi2c->w_size = 1;
    pi2c->r_size = 0;

    pi2c->w_data[0] = w_data;

    /* i2c command send */
    int ret = _i2c_dev_ioctl(pi2c);
    if (ret < 0)
        goto error;

    return 0;

error:
    return -1;
}

static int _i2c_read_one_byte(i2c_t *pi2c, uint8_t reg)
{
    if (pi2c == NULL)
    {
        ERR_MSG("Parameters point NULL.");
        goto error;
    }

    /* set reg */
    pi2c->reg = reg;

    pi2c->w_size = 0;
    pi2c->r_size = 1;

    /* i2c command send */
    int ret = _i2c_dev_ioctl(pi2c);
    if (ret < 0)
        goto error;

    return 0;

error:
    return -1;
}

///////////////////////////////////////////////////////////////////////////////

static int _gy521_set_init(gy521_t *pgy)
{
    if (pgy == NULL || pgy->pi2c == NULL)
    {
        ERR_MSG("Parameters point NULL.");
        goto error;
    }

    /* i2c open */
    i2c_t *pi2c = pgy->pi2c;
    int ret = _i2c_dev_open(pi2c);
    if (ret < 0)
        goto error;

    /* set PWR_MGMT_1 */
    ret = _i2c_read_one_byte(pi2c, PWR_MGMT_1);
    if (ret < 0)
        goto error;

    ret = _i2c_write_one_byte(pi2c, PWR_MGMT_1,
                              (pi2c->r_data[0]) & ~(0x1 << CYCLE) &
                                  ~(0x1 << SLEEP));
    if (ret < 0)
        goto error;

    /* set PWR_MGMT_2 */
    ret = _i2c_read_one_byte(pi2c, PWR_MGMT_2);
    if (ret < 0)
        goto error;

    ret = _i2c_write_one_byte(pi2c, PWR_MGMT_2,
                              (pi2c->r_data[0]) & ~(0x1 << STBY_XA) &
                                  ~(0x1 << STBY_YA) & ~(0x1 << STBY_ZA));
    if (ret < 0)
        goto error;

    /* i2c close */
    ret = _i2c_dev_close(pi2c);
    if (ret < 0)
        goto error;

    return 0;

error:
    return -1;
}

static int _gy521_set_accel(gy521_t *pgy)
{
    if (pgy == NULL || pgy->pi2c == NULL)
    {
        ERR_MSG("Parameters point NULL.");
        goto error;
    }

    /* i2c open */
    i2c_t *pi2c = pgy->pi2c;
    int ret = _i2c_dev_open(pi2c);
    if (ret < 0)
        goto error;

    /* set ACCEL_CONFIG */
    ret = _i2c_read_one_byte(pi2c, ACCEL_CONFIG);
    if (ret < 0)
        goto error;

    ret = _i2c_write_one_byte(pi2c, ACCEL_CONFIG, (pi2c->r_data[0]) & ~(0x7));
    if (ret < 0)
        goto error;

    /* set CONFIG */
    ret = _i2c_read_one_byte(pi2c, CONFIG);
    if (ret < 0)
        goto error;

    ret = _i2c_write_one_byte(pi2c, CONFIG, (pi2c->r_data[0]) & ~(0x7));
    if (ret < 0)
        goto error;

    /* i2c close */
    ret = _i2c_dev_close(pi2c);
    if (ret < 0)
        goto error;

    return 0;

error:
    return -1;
}

static int _gy521_set_motion_reg(gy521_t *pgy)
{
    if (pgy == NULL || pgy->pi2c == NULL)
    {
        ERR_MSG("Parameters point NULL.");
        goto error;
    }

    /* i2c open */
    i2c_t *pi2c = pgy->pi2c;
    int ret = _i2c_dev_open(pi2c);
    if (ret < 0)
        goto error;

    /* set motion interrupt to enable */
    ret = _i2c_write_one_byte(pi2c, INT_ENABLE, (0x1 << MOT_EN));
    if (ret < 0)
        goto error;

    /* set motion duration */
    ret = _i2c_write_one_byte(pi2c, MOT_DUR, 0x1);
    if (ret < 0)
        goto error;

    /* set motion threshold */
    ret = _i2c_write_one_byte(pi2c, MOT_THR, 20);
    if (ret < 0)
        goto error;

    /* i2c close */
    ret = _i2c_dev_close(pi2c);
    if (ret < 0)
        goto error;

    return 0;

error:
    return -1;
}

static int _gy521_set_freq_and_cycle(gy521_t *pgy)
{
    if (pgy == NULL || pgy->pi2c == NULL)
    {
        ERR_MSG("Parameters point NULL.");
        goto error;
    }

    /* i2c open */
    i2c_t *pi2c = pgy->pi2c;
    int ret = _i2c_dev_open(pi2c);
    if (ret < 0)
        goto error;

    /* set ACCEL_CONFIG */
    ret = _i2c_read_one_byte(pi2c, ACCEL_CONFIG);
    if (ret < 0)
        goto error;

    ret = _i2c_write_one_byte(pi2c, ACCEL_CONFIG, (pi2c->r_data[0]) | (0x7));
    if (ret < 0)
        goto error;

    /* set PWR_MGMT_2 */
    ret = _i2c_read_one_byte(pi2c, PWR_MGMT_2);
    if (ret < 0)
        goto error;

    ret = _i2c_write_one_byte(pi2c, PWR_MGMT_2,
                              (pi2c->r_data[0]) | (0x1 << LP_WAKE_CTRL_0));
    if (ret < 0)
        goto error;

    /* set PWR_MGMT_1 */
    ret = _i2c_read_one_byte(pi2c, PWR_MGMT_1);
    if (ret < 0)
        goto error;

    ret = _i2c_write_one_byte(pi2c, PWR_MGMT_1,
                              (pi2c->r_data[0]) | (0x1 << CYCLE));
    if (ret < 0)
        goto error;

    /* i2c close */
    ret = _i2c_dev_close(pi2c);
    if (ret < 0)
        goto error;

    return 0;

error:
    return -1;
}

#if 0
static int _gy521_gyro_scale(gy521_t *pgy)
{
	if (pgy == NULL || pgy->pi2c == NULL)
	{
		ERR_MSG("Parameters point NULL.");
		goto error;
	}

	/* i2c open */
	i2c_t *pi2c = pgy->pi2c;
	int ret = _i2c_dev_open(pi2c, I2C_NUM);
	if (ret < 0)
		goto error;

	/* set gyro scale */
	pi2c->addr = GY521_ADDR;
	pi2c->reg = 0x1b;

	pi2c->w_size = 1;
	pi2c->r_size = 0;

	pi2c->w_data[0] = 0xf8;

	/* i2c command send */
	ret = _i2c_dev_ioctl(pi2c);
	if (ret < 0)
		goto error;

	/* i2c close */
	ret = _i2c_dev_close(pi2c);
	if (ret < 0)
		goto error;

	return 0;

error :
	return -1;

	return 0;
}

static int _gy521_accerlator_scale(gy521_t *pgy)
{
	if (pgy == NULL || pgy->pi2c == NULL)
	{
		ERR_MSG("Parameters point NULL.");
		goto error;
	}

	/* i2c open */
	i2c_t *pi2c = pgy->pi2c;
	int ret = _i2c_dev_open(pi2c, 3);
	if (ret < 0)
		goto error;

	/* set accumulate scale */
	pi2c->addr = GY521_ADDR;
	pi2c->reg = 0x1c;

	pi2c->w_size = 1;
	pi2c->r_size = 0;

	pi2c->w_data[0] = 0xf8;

	/* i2c command send */
	ret = _i2c_dev_ioctl(pi2c);
	if (ret < 0)
		goto error;

	/* i2c close */
	ret = _i2c_dev_close(pi2c);
	if (ret < 0)
		goto error;

	return 0;

error :
	return -1;
}

static int _gy521_set_mot_int(gy521_t *pgy)
{
	if (pgy == NULL || pgy->pi2c == NULL)
	{
		ERR_MSG("Parameters point NULL.");
		goto error;
	}

	/* i2c open */
	i2c_t *pi2c = pgy->pi2c;
	int ret = _i2c_dev_open(pi2c, 3);
	if (ret < 0)
		goto error;

	/* read interrupt enable register */
	pi2c->addr = GY521_ADDR;
	pi2c->reg = 0x38;

	pi2c->w_size = 0;
	pi2c->r_size = 1;

	/* i2c command send */
	ret = _i2c_dev_ioctl(pi2c);
	if (ret < 0)
		goto error;

	/* set MOT_EN reg 1 */
	pi2c->w_size = 1;
	pi2c->r_size = 0;

	pi2c->w_data[0] = pi2c->r_data[0] | (0x1 << 6);

	/* i2c command send */
	ret = _i2c_dev_ioctl(pi2c);
	if (ret < 0)
		goto error;

	/* i2c close */
	ret = _i2c_dev_close(pi2c);
	if (ret < 0)
		goto error;

	return 0;

error :
	return -1;
}
#endif

static int _gy521_get_data(gy521_t *pgy)
{
    if (pgy == NULL)
    {
        ERR_MSG("Parameters point NULL.");
        goto error;
    }

    /* i2c open */
    i2c_t *pi2c = pgy->pi2c;
    int ret = _i2c_dev_open(pi2c);
    if (ret < 0)
        goto error;

    /* get gyro/accerlator data */
    pi2c->addr = GY521_ADDR;
    pi2c->reg = 0x3b;

    pi2c->w_size = 0;
    pi2c->r_size = GY521_GET_DATA_COUNT;

    /* i2c command send */
    ret = _i2c_dev_ioctl(pi2c);
    if (ret < 0)
        goto error;

    /* real computed log */
    ret = _gy521_data_print(pgy);
    if (ret < 0)
        goto error;

    /* i2c close */
    ret = _i2c_dev_close(pi2c);
    if (ret < 0)
        goto error;

    return 0;

error:
    return -1;
}

static int _gy521_data_print(gy521_t *pgy)
{
    if (pgy == NULL)
    {
        ERR_MSG("Parameters point NULL/");
        goto error;
    }

    i2c_t *pi2c = pgy->pi2c;

    // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)
    // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
    // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
    // 0x41 (TEMP_OUT_H) & 0x42 (TEMP_OUT_L)
    // 0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
    // 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
    // 0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)

    uint16_t ac_x;
    uint16_t ac_y;
    uint16_t ac_z;

    uint16_t temp;

    uint16_t gy_x;
    uint16_t gy_y;
    uint16_t gy_z;

    ac_x = (uint16_t)(pi2c->r_data[0]) << 8;
    ac_x |= (uint16_t)(pi2c->r_data[1]);
    ac_y = (uint16_t)(pi2c->r_data[2]) << 8;
    ac_y |= (uint16_t)(pi2c->r_data[3]);
    ac_z = (uint16_t)(pi2c->r_data[4]) << 8;
    ac_z |= (uint16_t)(pi2c->r_data[5]);

    temp = (uint16_t)(pi2c->r_data[6]) << 8;
    temp |= (uint16_t)(pi2c->r_data[7]);

    gy_x = (uint16_t)(pi2c->r_data[8]) << 8;
    gy_x |= (uint16_t)(pi2c->r_data[9]);
    gy_y = (uint16_t)(pi2c->r_data[10]) << 8;
    gy_y |= (uint16_t)(pi2c->r_data[11]);
    gy_z = (uint16_t)(pi2c->r_data[12]) << 8;
    gy_z |= (uint16_t)(pi2c->r_data[13]);

    printf("----------------- Get Information from GY521 ------------------\n");
    printf("AC_X : %d\tAC_Y : %d\tAC_Z : %d\t", ac_x, ac_y, ac_z);
    printf("Temp : %.2lf\t", (double)temp / 340.0 + 36.53);
    printf("GY_X : %d\tGY_Y : %d\tGY_Z : %d\t", gy_x, gy_y, gy_z);
    printf("\n\n");

    return 0;

error:
    return -1;
}

static int _gy521_init(gy521_t *pgy, int num)
{
    if (pgy == NULL)
    {
        ERR_MSG("Parameters point NULL.");
        goto error;
    }

    /* create i2c controller */
    pgy->pi2c = (i2c_t *)malloc(sizeof(i2c_t));
    if (pgy->pi2c == NULL)
    {
        ERR_MSG("Failed to allocate memory for i2c_t.");
        goto error;
    }

    /* set device address */
    pgy->pi2c->addr = GY521_ADDR;
    pgy->pi2c->num = num;

    int ret = -1;

    /* init gy521 */
    ret = _gy521_set_init(pgy);
    if (ret < 0)
        goto error;

    ret = _gy521_set_accel(pgy);
    if (ret < 0)
        goto error;

    ret = _gy521_set_motion_reg(pgy);
    if (ret < 0)
        goto error;

    /* need to dealy during 1ms */
    usleep(1000);

    ret = _gy521_set_freq_and_cycle(pgy);
    if (ret < 0)
        goto error;

#if 0
	ret = _gy521_set_mot_int(pgy);
	if (ret < 0)
		goto error;

	ret = _gy521_gyro_scale(pgy);
	if (ret < 0)
		goto error;

	ret = _gy521_accerlator_scale(pgy);
	if (ret < 0)
		goto error;
#endif

    return 0;

error:
    _gy521_destory(pgy);
    return -1;
}

static int _gy521_destory(gy521_t *pgy)
{
    if (pgy == NULL)
    {
        ERR_MSG("Parameters point NULL.");
        goto error;
    }

    /* i2c close */
    int ret = _i2c_dev_close(pgy->pi2c);
    if (ret < 0)
        goto error;

    /* desotry i2c controller */
    if (pgy->pi2c)
    {
        free((void *)pgy->pi2c);
        pgy->pi2c = NULL;
    }

    return 0;

error:
    return -1;
}

///////////////////////////////////////////////////////////////////////////////

#ifdef GPIO_CTRL
static int _gpio_export(unsigned int gpio_num)
{
    int fd = -1;
    fd = open(SYSFS_GPIO_DIR "/export", O_WRONLY);
    if (fd < 0)
    {
        ERR_MSG("Failed to open /export for gpio %d", gpio_num);
        goto error;
    }

    char buf[MAX_BUF];
    int len = snprintf(buf, sizeof(buf), "%d", gpio_num);
    int ret = pwrite(fd, (const void *)buf, (size_t)len, 0);
    if (ret != len)
    {
        ERR_MSG("Failed to write data");
        goto error;
    }

    close(fd);

    return 0;

error:
    _gpio_unexport(gpio_num);

    if (fd >= 0)
        close(fd);

    return -1;
}

static int _gpio_unexport(unsigned int gpio_num)
{
    int fd;
    char buf[MAX_BUF];

    fd = open(SYSFS_GPIO_DIR "/unexport", O_WRONLY);
    if (fd < 0)
    {
        ERR_MSG("Failed to open /unexport for gpio %d", gpio_num);
        goto error;
    }

    int len = snprintf(buf, sizeof(buf), "%d", gpio_num);
    int ret = pwrite(fd, buf, len, 0);
    if (ret != len)
    {
        ERR_MSG("Failed to write data.");
        goto error;
    }

    close(fd);

    return 0;

error:
    if (fd >= 0)
        close(fd);

    return -1;
}

static int _gpio_set_dir(unsigned int gpio_num)
{
    int fd;
    char buf[MAX_BUF];

    snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/gpio%d/direction", gpio_num);

    fd = open(buf, O_WRONLY);
    if (fd < 0)
    {
        ERR_MSG("Failed to open /direction for gpio %d", gpio_num);
        goto error;
    }

    int ret = pwrite(fd, "in", 3, 0);
    if (ret != 3)
    {
        ERR_MSG("Failed to write data.");
        goto error;
    }

    close(fd);

    return 0;

error:
    if (fd >= 0)
        close(fd);

    return -1;
}

static int _gpio_open(unsigned int gpio_num, int *pfd)
{
    if (pfd == NULL)
    {
        ERR_MSG("Parameters point NULL.");
        goto error;
    }

    int fd;
    char buf[MAX_BUF];

    snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/gpio%d/value", gpio_num);

    fd = open(buf, O_RDONLY | O_NONBLOCK);
    if (fd < 0)
    {
        ERR_MSG("Failed to open gpio %d.", gpio_num);
        goto error;
    }

    *pfd = fd;

    return 0;

error:
    return -1;
}

static int _gpio_close(int *pfd)
{
    if (pfd == NULL)
    {
        ERR_MSG("Parameters point NULL.");
        goto error;
    }

    if (*pfd != -1)
    {
        close(*pfd);
        *pfd = -1;
    }

    return 0;

error:
    return -1;
}

static int _gpio_get_val(unsigned int gpio_num)
{
    char buf[MAX_BUF];
    snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/gpio%d/value", gpio_num);

    int fd = -1;
    fd = open(buf, O_RDONLY);
    if (fd < 0)
    {
        ERR_MSG("Failed to read gpio %d data.", gpio_num);
        goto error;
    }

    int ret = pread(fd, buf, 1, 0);
    if (ret != 1)
    {
        ERR_MSG("Failed to read data.");
        goto error;
    }
    close(fd);

    return buf[0] == '0' ? 0 : 1;

error:
    if (fd >= 0)
        close(fd);

    return -1;
}
#endif

///////////////////////////////////////////////////////////////////////////////

/**
 *	argv[1] -> gpio number
 *	How to compute gpio number?
 *	On the MRB, input command as "cat /sys/firmware/acpi/tables/DSDT"
 *	And, find "General Purpose Input/Output (GPIO) Controller - " in DSDT.
 *	After these strings, you can find words such as "North / Northwest /
 *South / Southwest". This order is index. For example North is 0, Southwest
 *is 3.
 *
 *	And, input command as "cat /d/gpio".
 *	As this log, you can find start gpiochip%d number.
 *
 *	gordon_peak:/d # cat gpio
 *	gpiochip3: GPIOs 267-309, parent: platform/INT3452:03, INT3452:03:
 *	gpio-293 (                    |0000:00:1b.0 cd     ) in  hi IRQ
 *	gpiochip2: GPIOs 310-356, parent: platform/INT3452:02, INT3452:02:
 *	gpiochip1: GPIOs 357-433, parent: platform/INT3452:01, INT3452:01:
 *	gpiochip0: GPIOs 434-511, parent: platform/INT3452:00, INT3452:00:
 *	gpio-444 (                    |sysfs               ) out hi
 *	gpio-456 (                    |ADV7481_HDMI_IRQ    ) in  hi IRQ
 *
 *  If you try to control Southwest GPIO 28, virtual gpio number is (267 + 28).
 */

int main(int argc, char *argv[])
{
    int ret = -1;
    gy521_t *pgy = NULL;

    if (argc <= 1)
    {
        ERR_MSG("Error input parameters number.");
        goto error;
    }

    pgy = (gy521_t *)malloc(sizeof(gy521_t));
    if (pgy == NULL)
    {
        ERR_MSG("Failed to allocate memory for gy521_t.");
        goto error;
    }

    ret = _gy521_init(pgy, atoi(argv[1]));
    if (ret < 0)
        goto error;

#if 0
	unsigned int gpio_num = (unsigned int)atoi(argv[1]);
	ret = _gpio_export(gpio_num);
	if (ret < 0)
		goto error;

	ret = _gpio_set_dir(gpio_num);
	if (ret < 0)
		goto error;

	int fd = -1;
	ret = _gpio_open(gpio_num, &fd);
	if (ret < 0)
		goto error;

	while (1)
	{
		usleep(100000);

		ret = _gpio_get_val(gpio_num);
		if (ret < 0)
			goto error;

		if (ret == 0)
			continue;

		ret = _gy521_get_data(pgy);
		if (ret < 0)
			goto error;
	}

	ret = _gpio_close(&fd);
	if (ret < 0)
		goto error;

	ret = _gpio_unexport(gpio_num);
	if (ret < 0)
		goto error;

#else
    //	int count = 3000;
    //	while (count--)
    {
        ret = _gy521_get_data(pgy);
        if (ret < 0)
            goto error;

        //		usleep(100000);
    }
#endif

error:
    _gy521_destory(pgy);

    if (pgy)
    {
        free((void *)pgy);
        pgy = NULL;
    }

    return 0;
}
