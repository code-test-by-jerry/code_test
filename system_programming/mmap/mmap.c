#include <stdio.h>
#include <stdlib.h>

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <sys/mman.h>

#include <errno.h>

#define MAP_SIZE 4096UL
#define MAP_MASK (MAP_SIZE - 1)

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        printf("Inavalid parameters!!\n");
        return -EPERM;
    }

    unsigned long target = strtoul(argv[1], NULL, 16);
    printf("%lx\n", target);

    int fd = open("/dev/mem", O_RDWR | O_SYNC);
    if (fd < 0)
    {
        printf("Failed to open /dev/mem\n");
        return -errno;
    }

    //	printf("0x%lx 0x%lx\n", MAP_MASK, ~MAP_MASK);

    char *map_base = mmap(0, MAP_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd,
                          target & ~MAP_MASK);
    if (map_base == MAP_FAILED)
    {
        printf("Failed to mapping virtual address.\n");
        goto escape;
    }

    char *virt_addr = map_base + (target & MAP_MASK);

    unsigned long i;
    unsigned long len = 1024;
    for (i = 0; i < len; i++)
    {
        printf("[0x%lx] Value : 0x%x\n", target + i * sizeof(int),
               *((volatile unsigned int *)(virt_addr) + i));
    }

    int ret = munmap(map_base, MAP_SIZE);
    if (ret < 0)
        printf("Failed to ummap,,\n");

escape:
    close(fd);

    return 0;
}
