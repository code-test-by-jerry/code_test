#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

typedef union {
    uint64_t val;
    struct
    {
        uint32_t lo;
        uint32_t hi;
    };

} msr_t;

static inline uint64_t _read_msr(volatile uint32_t idx)
{
    msr_t msr;

    asm volatile("rdmsr" : "=a"(msr.lo), "=d"(msr.hi) : "c"(idx));
    return msr.val;
}

static inline void _write_msr(volatile uint32_t idx, uint64_t val)
{
    msr_t msr = {.val = val};

    asm volatile("wrmsr" : : "c"(idx), "a"(msr.lo), "d"(msr.hi));
}

static inline void _get_cpuid(uint32_t *eax, uint32_t *ebx, uint32_t *ecx,
                              uint32_t *edx)
{
    asm volatile("cpuid"
                 : "=a"(*eax), "=b"(*ebx), "=c"(*ecx), "=d"(*edx)
                 : "0"(*eax), "2"(*ecx));
}

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        printf("Failed to executing the msr application!\n");
        printf("ex) ./msr 0x123\n");
        printf("ex) ./msr 0x123 0x456\n");
        return -EPERM;
    }

#if 1
    volatile uint32_t addr = (volatile uint32_t)strtoul(argv[1], NULL, 16);
    unsigned long write_data = 0;
    if (argc == 3)
    {
        printf("Write!\n");
        write_data = strtoul(argv[2], NULL, 16);
        printf("write: 0x%08lx\n", write_data);
        _write_msr(addr, write_data);
    }
    else
    {
        printf("Read!\n");
        unsigned long read_data = _read_msr(addr);
        printf("read: 0x%08lx\n", read_data);
    }
#else
    uint32_t test[5] = {
        0,
    };
    _get_cpuid(&test[0], &test[1], &test[3], &test[2]);

    printf("0x%0x\n", test[0]);
    printf("0x%0x\n", test[1]);
    printf("0x%0x\n", test[2]);
    printf("0x%0x\n", test[3]);
#endif

    return 0;
}
