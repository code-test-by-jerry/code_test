#include <stdio.h>
#include <stdlib.h>

#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

int main(void)
{
    fd_set for_read;
    struct timeval timeout;

    FD_ZERO(&for_read);
    FD_SET(0, &for_read);

    timeout.tv_sec = -1;
    timeout.tv_usec = 0;

    //	int ret = select(1, &for_read, NULL, NULL, NULL);
    int ret = select(1, &for_read, NULL, NULL, &timeout);
    if (ret < 0)
    {
        printf("select error!!\n");
        exit(-1);
    }
    else if (ret == 0)
    {
        printf("timeout select!!\n");
    }
    else if (ret)
    {
        printf("select fd!!\n");

        char buf[64] = {
            0,
        };
        read(0, buf, sizeof(buf) + 1);
        printf("%s\n", buf);
    }

    return 0;
}
