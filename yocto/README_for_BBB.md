# How to setup the environment of yocto on the Beaglebone Black


## Prerequisite

  * Download the poky

  ```bash
  $ git clone git://git.yoctoproject.org/poky
  $ cd poky && ls -al
  ```

  * Change the branch to the released stable branch

  ```bash
  $ git checkout zeus
  $ git branch
    master
  * zeus
  ```

  * Setup the environment for the beaglebone black

  ```bash
  $ source oe-init-build-env build_beagle_bone_black
  $ vim conf/local.conf
  MACHINE ?= "beaglebone-yocto"
  DL_DIR ?= "${TOPDIR}/downloads"
  SSTATE_DIR ?= "${TOPDIR}/sstate-cache"
  TMPDIR = "${TOPDIR}/tmp"
  DISTRO ?= "poky"
  PACKAGE_CLASSES ?= "package_rpm"
  EXTRA_IMAGE_FEATURES ?= "debug-tweaks"
  USER_CLASSES ?= "buildstats image-mklibs image-prelink"
  PATCHRESOLVE = "noop"
  BB_DISKMON_DIRS ??= "\
    STOPTASKS,${TMPDIR},1G,100K \
    STOPTASKS,${DL_DIR},1G,100K \
    STOPTASKS,${SSTATE_DIR},1G,100K \
    STOPTASKS,/tmp,100M,100K \
    ABORT,${TMPDIR},100M,1K \
    ABORT,${DL_DIR},100M,1K \
    ABORT,${SSTATE_DIR},100M,1K \
    ABORT,/tmp,10M,1K"
  PACKAGECONFIG_append_pn-qemu-system-native = " sdl"
  CONF_VERSION = "1"
  ```


## Build the image for the BBB

  * Saved the build log using bitbake,tee command

  ```bash
  $ bitbake -DDD core-image-minimal 2>&1 | tee build.log
  ```

  * After the build, can check the built images as below directory

  ```bash
  $ cd tmp/deploy/images/beaglebone-yocto && ls -al
  ```

  1. ***MLO*** : first bootloader
  2. ***u-boot.img***: secondary bootloader
  3. ***zImage***: Kernel image
  4. ***am335x-bone-beaglebone-yocto.dtb***: device tree
  5. ***modules-beaglebone-yocto.tgz***: Kernel modules
  6. ***core-image-minimal-beaglebone-yocto.tar.bz2***: root file system


## Make the sector on the SDcard for the booting

  * Please refer to the script below

  ```bash
  $ ./setup_sd_for_bbb.sh <output directory path>
  ```


## Execute the ***kdb*** and ***kgdb*** on the Yocto kernel

  * First, Need to Kernel configuration as below

  ```bash
  CONFIG_KGDB_KDB=y
  CONFIG_KDB_DEFAULT_ENABLE=0x1
  CONFIG_KDB_CONTINUE_CATASTROPHIC=y
  CONFIG_HAVE_ARCH_KGDB=y
  CONFIG_KGDB=y
  CONFIG_KGDB_SERIAL_CONSOLE=y
  CONFIG_KGDB_KDB=y
  ```

  * One device divided two-terminal using the agent-proxy tool

  ```bash
  $ git clone https://kernel.googlesource.com/pub/scm/utils/kernel/kgdb/agent-proxy.git
  $ cd agent-proxy && make
  $ sudo -E ./agent-proxy 1234^5678 0 /dev/ttyUSB0,115200
  Agent Proxy 1.97 Started with: 1234^5678 0 /dev/ttyUSB0,115200
  Agent Proxy running. pid: 8671
  ```

  * Connect the beaglebone on the localhost using telnet

  ```console
  $ telnet localhost 1234
  Trying 127.0.0.1...
  Connected to localhost.
  Escape character is '^]'.

  root@beaglebone-yocto:~#
  ```

  * How to enter the ***kdb*** on the Yocto shell,

  ```console
  root@beaglebone-yocto:~# tty
  /dev/ttyS0
  root@beaglebone-yocto:~# echo ttyS0 > /sys/module/kgdboc/parameters/kgdboc
  KGDB: Registered I/O driver kgdboc
  root@beaglebone-yocto:~# echo g > /proc/sysrq-trigger
  sysrq: DEBUG

  Entering kdb (current=0xde268000, pid 273) due to Keyboard Entry
  kdb> kgdb
  Entering please attach debugger or use $D#44+ or $3#33
  ```

  * On the localhost, execute the gdb using vmlinux for ***kgdb***

  ```bash
  $ arm-none-eabi-gdb vmlinux
  (gdb) target remote localhost:5678
  Remote debugging using localhost:5678
  (gdb) bt
  (gdb) info frame
  (gdb) info register
  (gdb) monitor dmesg
  ```

  * How to disconnect between ***kdb*** and ***kgdb***

  ```bash
  (gdb) maint packet 3
  or
  (gdb) q
  ```

  * Enjoy :)
