# How to setup the environment of yocto on the Raspberry Pi


## Prerequisite

  * I used the raspberry pi3 B+ board (ARMv8, 64bit)

  * First, Download the needed the opensource

  ```bash
  $ cd <root directory>
  $ git clone git://git.yoctoproject.org/poky
  $ git clone git://git.yoctoproject.org/meta-raspberrypi
  $ git clone git://git.openembedded.org/meta-openembedded
  ```

  * Second, setup the same branch each directory to success the building

  ```bash
  $ cd poky && git checkout zeus && cd -
  $ cd meta-raspberrypi && git checkout zeus && cd -
  $ cd meta-openembedded && git checkout zeus && cd -
  ```

  * Add the layer to build the raspberry pi

  ```bash
  $ cd poky && ls -al
  $ source oe-init-build-env build_raspberry
  $ bitbake-layers add-layer ../../meta-raspberrypi
  $ bitbake-layers add-layer ../../meta-openembedded/meta-oe
  $ bitbake-layers add-layer ../../meta-openembedded/meta-python
  $ bitbake-layers add-layer ../../meta-openembedded/meta-multimedia
  $ bitbake-layers add-layer ../../meta-openembedded/meta-networking
  ```

  * After that, can check the layers below commands

  ```bash
  $ vim conf/bblayers.conf
  or
  $ bitbake-layers show-layer
  ```

  * Setup the environment for the raspberry pi (more option refer to the README_for_BBB.md)

  ```bash
  $ vim conf/local.conf
  MACHINE ?= "raspberrypi3-64"
  ENABLE_UART= "1" # UART enable flag
  ```


## Build the image for the Raspberry Pi

  * Check the bitbake parameter on the meta-raspberrypi

  ```bash
  $ cd <root directory>/meta-raspberrypi/recipes-core/images && ls
  rpi-basic-image.bb rpi-hwup-image.bb rpi-test-image.bb
  ```

  * Using the "rpi-basic-image" bitbake file for using basic packages such as ssh

  ```bash
  $ bitbake rpi-basic-image
  or
  $ bitbake -DDD rpi-basic-image 2>&1 | tee build.log # for logging
  ```

  * After the build, can check the built images as below directory

  ```bash
  $ cd tmp/deploy/images/raspberrypi3-64 && ls -al
  ```

  1. ***Image***: Kernel image
  2. ***bcm2837-rpi-3-b-raspberrypi3-64.dtb***: main device tree
  3. ***modules-raspberrypi3-64.tgz***: Kernel modules
  4. ***rpi-basic-image-raspberrypi3-64.tar.bz2***: root file system
  5. ****.dtbo***: sub device tree, should be located in the overlay folder


  * Can see also bootloader as below folder

  ```bash
  $ cd bcm2835-bootfiles && ls -al
  ```

  1. ***bootcode.bin*** : first bootloader
  2. ***startx.elf***: secondary bootloader
  3. ***fixupx.dat***: For set up the SDRAM partition between GPU and CPU
  4. ***cmdline.txt***: Kernel cmdline
  5. ***config.txt***: boot config file


## Make the sector on the SDcard for the booting

  * Please refer to the script below

  ```bash
  $ ./setup_sd_for_pi.sh <output directory path>
  or
  $ sudo dd if=rpi-basic-image-raspberrypi3-64.rpi-sdimg of=/dev/mmcblk0 bs=4M status=progress
  ```


## Build the SDK for the GDB on the Raspberry Pi

  * Download and setup the SDK

  ```bash
  $ bitbake meta-toolchain
  $ cd tmp/deploy/sdk
  $ ./poky-glibc-x86_64-meta-toolchain-aarch64-raspberrypi3-64-toolchain-3.0.4.sh
  ```

  * Setup the envrionment for using the GDB

  ```bash
  $ source /opt/poky/3.0.4/environment-setup-aarch64-poky-linux
  $ echo $GDB
  aarch64-poky-linux-gdb
  ```


## Execute the ***kdb*** and ***kgdb*** on the Yocto kernel

  * First, Need to Kernel configuration as below

  ```bash
  CONFIG_KGDB_KDB=y
  CONFIG_KDB_DEFAULT_ENABLE=0x1
  CONFIG_KDB_CONTINUE_CATASTROPHIC=y
  CONFIG_HAVE_ARCH_KGDB=y
  CONFIG_KGDB=y
  CONFIG_KGDB_SERIAL_CONSOLE=y
  CONFIG_KGDB_KDB=y
  ```

  * One device divided two-terminal using the agent-proxy tool

  ```bash
  $ git clone https://kernel.googlesource.com/pub/scm/utils/kernel/kgdb/agent-proxy.git
  $ cd agent-proxy && make
  $ sudo -E ./agent-proxy 1234^5678 0 /dev/ttyUSB0,115200
  Agent Proxy 1.97 Started with: 1234^5678 0 /dev/ttyUSB0,115200
  Agent Proxy running. pid: 8671
  ```

  * Connect the Raspberry Pi on the localhost using telnet

  ```console
  $ telnet localhost 1234
  Trying 127.0.0.1...
  Connected to localhost.
  Escape character is '^]'.

  root@raspberrypi3-64:~#
  ```

  * How to enter the ***kdb*** on the Yocto shell,

  ```console
  root@raspberrypi3-64:~# tty
  /dev/ttyS0
  root@raspberrypi3-64:~# echo ttyS0,115200 > /sys/module/kgdboc/parameters/kgdboc
  KGDB: Registered I/O driver kgdboc
  root@raspberrypi3-64:~# echo g > /proc/sysrq-trigger
  sysrq: DEBUG

  Entering kdb (current=0xde268000, pid 273) due to Keyboard Entry
  kdb> kgdb
  Entering please attach debugger or use $D#44+ or $3#33
  ```

  * On the localhost, execute the gdb using vmlinux for ***kgdb***

  ```bash
  $ aarch64-poky-linux-gdb vmlinux
  (gdb) target remote localhost:5678
  Remote debugging using localhost:5678
  (gdb) bt
  (gdb) info frame
  (gdb) info register
  (gdb) monitor dmesg
  ```

  * How to disconnect between ***kdb*** and ***kgdb***

  ```bash
  (gdb) maint packet 3
  or
  (gdb) q
  ```


## Appendix

  * When using the command of the "backtrace" in the gdb, you met "??"

  ```console
  (gdb) bt
  #0  0xffffffa74f95cbb8 in ?? ()
  #1  0xffffffa74fd22f0c in ?? ()
  #2  0x0000005568a4a760 in ?? ()
  Backtrace stopped: previous frame inner to this frame (corrupt stack?)
  ```

  * It need to change the option to disable the randomization

  ```bash
  $ cd <root directory>/poky
  $ source oe-init-build-env build_raspberry
  $ bitbake -c menuconfig virtual/kernel
  ```

  * After that, off the option of the CONFIG_RANDOMIZE_BASE and rebuild the Kernel

  ```bash
  $ bitbake virtual/kernel
  or
  $ bitbake -c cleansstate virtual/kernel # clean build
  ```

  * If you use the ***cmdline.txt***, add the ***nokaslr***

  ```bash
  $ vim tmp/deploy/images/raspberrypi3-64/bcm2835-bootfiles/cmdline.txt
  dwc_otg.lpm_enable=0 console=serial0,115200 root=/dev/mmcblk0p2 rootfstype=ext4 rootwait nokaslr
  ```
