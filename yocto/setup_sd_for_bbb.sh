#!/bin/bash

DEV="/dev/mmcblk0"
PART_1="p1"
PART_2="p2"

# 32MB, default 512B
BOOT_SIZE=$((32 * 1024 * 1024 / 512))

BOOT_1="MLO"
BOOT_2="u-boot.img"
KERNEL_IMAGE="zImage"
KERNEL_MODULE="modules-beaglebone-yocto.tgz"
ROOTFS="core-image-minimal-beaglebone-yocto.tar.bz2"
DTB="am335x-bonegreen-beaglebone-yocto.dtb"
ENV="uEnv.txt"

BOOT_NAME="BOOT"
ROOTFS_NAME="ROOTFS"
MEDIA_DIR="/media/${USER}"

function check_file()
{
	if [ ! -e "$1" ]
	then
		echo "Cannot find the $1!"
		exit 1
	fi
}

function unmount_directory()
{
	ret=$(mount | grep "${MEDIA_DIR}/$1")
	if [[ "${ret}" != "" ]]
	then
		sudo umount -flv "${MEDIA_DIR}/$1"
		sync
		sudo rm -rf "${MEDIA_DIR}/$1"
	fi
}

function mount_directory()
{
	sudo mkdir -p "${MEDIA_DIR}/$1"
	sudo mount -v "${DEV}$2" "${MEDIA_DIR}/$1"
	sync
}

if [ "$#" -eq "0" ]
then
	echo "Need to parameter for the images path!"
	exit 1
fi

OUTPUT="$1"
if [[ "${OUTPUT: -1}" == "/" ]]
then
	OUTPUT="${OUTPUT:0:-1}"
fi

# file check
check_file ${DEV}
check_file "${OUTPUT}/${BOOT_1}"
check_file "${OUTPUT}/${BOOT_2}"
check_file "${OUTPUT}/${KERNEL_IMAGE}"
check_file "${OUTPUT}/${KERNEL_MODULE}"
check_file "${OUTPUT}/${ROOTFS}"
check_file "${OUTPUT}/${DTB}"

# unmount if used the partition
unmount_directory ${BOOT_NAME}
unmount_directory ${ROOTFS_NAME}

# clear partition information
sudo dd if=/dev/zero of=${DEV} bs=512 count=1 && sync

# setup the partition
# BOOT 32M FAT32 bootflag
# ROOTFS all ext4
sudo sfdisk --force ${DEV} << EOF
,${BOOT_SIZE},c,*
,,,
EOF
sync

# create the file system to the FAT32 for the BOOT in the partition 1
sudo mkfs.vfat -F 32 -n ${BOOT_NAME} ${DEV}${PART_1} && sync

# create the file system to the EXT4 for the ROOTFS in the partition 2
sudo mkfs.ext4 -F -L ${ROOTFS_NAME} ${DEV}${PART_2} && sync

# mount the directory
mount_directory ${BOOT_NAME} ${PART_1}
mount_directory ${ROOTFS_NAME} ${PART_2}

# print status
lsblk | grep "$(basename ${DEV})"

# copy images to the BOOT partition
sudo cp "${OUTPUT}/${BOOT_1}" "${MEDIA_DIR}/${BOOT_NAME}"
sudo cp "${OUTPUT}/${BOOT_2}" "${MEDIA_DIR}/${BOOT_NAME}"
sudo cp "${OUTPUT}/${KERNEL_IMAGE}" "${MEDIA_DIR}/${BOOT_NAME}"
sudo cp "${OUTPUT}/${DTB}" "${MEDIA_DIR}/${BOOT_NAME}"
sudo cp "${ENV}" "${MEDIA_DIR}/${BOOT_NAME}"

# unzip the image to the ROOTFS partition
sudo tar -xf "${OUTPUT}/${ROOTFS}" -C "${MEDIA_DIR}/${ROOTFS_NAME}"
sudo tar -xzf "${OUTPUT}/${KERNEL_MODULE}" -C "${MEDIA_DIR}/${ROOTFS_NAME}"
sudo chown -R root:root "${MEDIA_DIR}/${ROOTFS_NAME}"
sync

# unmount if used the partition
unmount_directory ${BOOT_NAME}
unmount_directory ${ROOTFS_NAME}

echo "Done."
